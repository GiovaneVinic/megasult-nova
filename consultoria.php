<?php 
include 'data/config.php';

$titulo = "Consultoria";
$bg = "dev/img/bread_background.jpg";
$con = $_GET['con'];

?>
<!DOCTYPE html>
<html lang="pt-BR">

<?php 
include 'includes/head.php'; 
?>
<body>

<div class="super_container">

<?php 
include 'includes/header.php'; 
?>
<?php 
include 'includes/breadcrumb.php';
?>
<div class="container">
	<nav class="software-menu">
	    <ul>
	    	<div class="row alinhar-vertical">
		        <li class="<?= ($con == 'gestao') ? 'active':''; ?> col-lg-3 col-md-3 col-sm-6">
		       		<a href="consultoria.php?con=gestao" title="Consultoria Empresarial">
		        		<figure class="justify-content-center d-flex">
							<img src="dev/img/icones/iponto.png" width="40" height="40" alt="Consultoria Empresarial">
						</figure>
						<p>Consultoria Empresarial</p>
					</a>
				</li>
				<li class="<?= ($con == 'societaria') ? 'active':''; ?> col-lg-3 col-md-3 col-sm-6">
		       		<a href="consultoria.php?con=societaria" title="Consultoria Societária">
		        		<figure class="justify-content-center d-flex">
							<img src="dev/img/icones/iponto.png" width="40" height="40" alt="Consultoria Societária">
						</figure>
						<p>Consultoria Societária</p>
					</a>
				</li>
				<li class="<?= ($con == 'tributaria') ? 'active':''; ?> col-lg-3 col-md-3 col-sm-6">
		       		<a href="consultoria.php?con=tributaria" title="Consultoria Tributária">
		        		<figure class="justify-content-center d-flex">
							<img src="dev/img/icones/iponto.png" width="40" height="40" alt="Consultoria Tributária">
						</figure>
						<p>Consultoria Tributária</p>
					</a>
				</li>
			</div>
	    </ul>
	</nav>
</div>

<?php 
	if ($con == 'gestao') {
		include 'includes/consultoria/gestao.php';
	}else if ($con == 'tributaria') {
		include 'includes/consultoria/tributaria.php';
	}else if ($con == 'societaria') {
		include 'includes/consultoria/societaria.php';
	}
?>

</div>
<?php 
	include 'includes/unidades.php';
	include 'includes/footer.php';
	include 'includes/scripts.php';
?>
</body>
</html>