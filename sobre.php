<?php 
include 'data/config.php';

$titulo = "A Megasult";
$bg = "dev/img/bread_background.jpg";
$sob = 'empresa';

?>
<!DOCTYPE html>
<html lang="pt-BR">

<?php 
include 'includes/head.php'; 
?>
<body>

<div class="super_container">

<?php 
include 'includes/header.php'; 
?>
<?php 
include 'includes/breadcrumb.php';
?>

<?php 
	if ($sob == 'empresa') {
		include 'includes/sobre/empresa.php';
	}
?>

</div>
<?php 
	include 'includes/unidades.php';
	include 'includes/footer.php';
	include 'includes/scripts.php';
?>
<script>
	$(document).on('click', '.collap', function(){
		var $icone = $(this).children('i')

		if($icone.hasClass('rotate'))
		{
			$icone.removeClass('rotate');
		}
		else
		{
			$icone.addClass('rotate');	
		}
	});
</script>
<script>
	var div = document.getElementsByClassName("curriculop")[0];
	var input = document.getElementById("curriculo");

	div.addEventListener("click", function(){
	    input.click();
	});
	input.addEventListener("change", function(){
	    var nome = "Não há arquivo selecionado. Selecionar arquivo...";
	    if(input.files.length > 0) nome = input.files[0].name;
	    div.innerHTML = nome;
	});
</script>
</body>
</html>