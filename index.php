<?php 
include 'data/config.php';
?>
<!DOCTYPE html>
<html lang="pt-BR">

<?php 
include 'includes/head.php'; 
?>

<body>

<div class="super_container">

<?php 
include 'includes/header.php'; 
?>

	<!-- Home -->

	<div class="home">
		<div class="home_background_container prlx_parent">
			<div class="home_background prlx" style="background-image:url(dev/img/slider_background.jpg)"></div>
		</div>
		
		<!-- Hero Slider -->
		<div class="hero_slider_container">
			
			<!-- Slider -->
			<div class="owl-carousel owl-theme hero_slider">

				<!-- Slider Item -->
				<div class="owl-item hero_slider_item item_1 d-flex flex-column align-items-center justify-content-center">
					<div class="container text-align-center">
						<span></span>
						<span></span>
						<span>CONTABILIDADE <br> FISCAL</span><br><br>
						<span>soluções completas para gestão da sua empresa</span>
						<br><br>
						<div class="botao-servicos">
							<div class="button-slider">
								<a href="contabilidade.php?ser=Escrituração%20Fiscal" class="trans_200">VER MAIS</a>
							</div>
						</div>
					</div>
				</div>

			</div>
			
			<!-- Hero Slider Navigation Left -->
			<div class="hero_slider_nav hero_slider_nav_left">
				<div class="hero_slider_prev d-flex flex-column align-items-center justify-content-center trans_200">
					<i class="fas fa-chevron-left trans_200"></i>
				</div>
			</div>

			<!-- Hero Slider Navigation Right -->
			<div class="hero_slider_nav hero_slider_nav_right">
				<div class="hero_slider_next d-flex flex-column align-items-center justify-content-center trans_200">
					<i class="fas fa-chevron-right trans_200"></i>
				</div>
			</div>

		</div>

		<div class="hero_side_text_container">
			<div class="double_arrow_container d-flex flex-column align-items-center justify-content-center">
				<div class="double_arrow nav_links" data-scroll-to=".icon_boxes">
					<i class="fas fa-chevron-left trans_200"></i>
					<i class="fas fa-chevron-left trans_200"></i>
				</div>
			</div>
			<div class="hero_side_text">
				<h2>OFERECEMOS SOLUÇÕES QUE OTIMIZAM OS NEGÓCIOS</h2>
				<p>Voltada às necessidades de sua empresa a Megasult é o resultado da união de profissionais experientes em consultoria, nas áreas de Contabilidade Gerencial, Economia, Administração, Informática e Propriedade Industrial.</p>
			</div>
		</div>
		
		<div class="next_section_scroll">
			<div class="next_section nav_links" data-scroll-to=".icon_boxes">
				<i class="fas fa-chevron-down trans_200"></i>
				<i class="fas fa-chevron-down trans_200"></i>
			</div>
		</div>
			
	</div>

	<!-- Icon Boxes -->

	<div class="icon_boxes">
		<div class="container_megasult">
			<div class="row">
				<div class="col text-center">
					<div class="section_title pb-30">
						<h1>Soluções completas para gestão da sua empresa.</h1>
					</div>
				</div>
			</div>
			<div class="row">

				<div class="col-xl-3 col-lg-6 icon_box_col">
					
					<div class="icon_box_item">
						<figure>
							<img src="dev/img/icones/contabilidade-fiscal.png" alt="Contabilidade Fiscal">
						</figure>
						<h2>Contabilidade</h2>
						<p class="text-justify">Escrituração contábil e fiscal, de acordo com a legislação vigente, bem como o cumprimento das obrigações acessórias pertinentes.</p>
						<div class="botao-servicos">
							<div class="button icon_box_button trans_200">
								<a href="contabilidade.php?ser=Auditoria%20Interna%20e%20Externa" class="trans_200">VER MAIS</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-xl-3 col-lg-6 icon_box_col">
					
					<!-- Icon Box Item -->
					<div class="icon_box_item">
						<figure>
							<img src="dev/img/icones/consultoria.png" alt="Consultoria Gerencial">
						</figure>
						<h2>Consultoria</h2>
						<p class="text-justify">Estudo da capacidade da empresa gerar lucros. Identificação do Ponto de Equilíbrio Econômico e Financeiro. Necessidade de Capital de Giro.</p>
						<div class="botao-servicos">
							<div class="button icon_box_button trans_200">
								<a href="consultoria.php?con=gestao" class="trans_200">VER MAIS</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-xl-3 col-lg-6 icon_box_col">
					
					<!-- Icon Box Item -->
					<div class="icon_box_item">
						<figure>
							<img src="dev/img/icones/erp.png" alt="Sistema ERP">
						</figure>
						<h2>Software</h2>
						<p class="text-justify">Especializada no fornecimento de consultoria em sistemas de gestão e implementação de projetos de TI (Tecnologia de Informação) para o mercado corporativo.</p>
						<div class="botao-servicos">
							<div class="button icon_box_button trans_200">
								<a href="software.php?ser=Amplus" class="trans_200">VER MAIS</a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-xl-3 col-lg-6 icon_box_col">
					
					<!-- Icon Box Item -->
					<div class="icon_box_item">
						<figure>
							<img src="dev/img/icones/patentes.png" alt="Marcas e Patentes">
						</figure>
						<h2>Registro de Marcas</h2>
						<p class="text-justify">A Divisão de Marcas e Patentes da Megasult visa auxiliá-lo na proteção de um dos maiores e mais importantes patrimônios: as suas marcas e invenções.</p>
						<div class="botao-servicos">
							<div class="button icon_box_button trans_200">
								<a href="registro.php?ser=Código%20de%20Barras" class="trans_200">VER MAIS</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Vertical Slider Section -->

	<div class="v_slider_section">
		<div class="container_megasult fill_height">
			<div class="row fill_height alinhar-vertical">
				<div class="col-lg-3 v_slider_section_image">
					<div class="v_slider_image">
						<img src="dev/img/software1.jpg" alt="Sistema ERP">
					</div>
				</div>
				<div class="col-lg-9 sistemas">
					<div class="row alinhar-vertical">
						<div class="col-lg-12">
							<h1 class="text-center mb-60 pt-30">Sua empresa no mundo digital</h1>
						</div>
						<div class="col-lg-6 icon_box_col">
							<div class="icon_box_item">
								<figure>
									<img src="dev/img/icones/amplus.png" alt="Amplus">
								</figure>
								<h2>Amplus</h2>
								<p class="text-justify p06">O Amplus é um sistema completo destinado a gestão e controle de frequência dos colaboradores de sua organização.</p>
								<div class="botao-servicos">
									<div class="button icon_box_button trans_200">
										<a href="software.php?ser=Amplus" class="trans_200">VER MAIS</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-6 icon_box_col">
							<div class="icon_box_item">
								<figure>
									<img src="dev/img/icones/iponto.png" alt="iPonto">
								</figure>
								<h2>Controle de Ponto</h2>
								<p class="text-justify p06">O iPonto é um sistema completo destinado a gestão e controle de frequência dos colaboradores de sua organização.</p>
								<div class="botao-servicos">
									<div class="button icon_box_button trans_200">
										<a href="software.php?ser=Controle%20de%20Ponto" class="trans_200">VER MAIS</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-6 icon_box_col">
							<div class="icon_box_item">
								<figure>
									<img src="dev/img/icones/linx.png" alt="Linx">
								</figure>
								<h2>Linx</h2>
								<p class="text-justify p06">Nossos sistemas para lojas de roupas e acessórios, possibilitam a compreensão da jornada do consumidor, buscando aperfeiçoar as estratégias do seu negócio.</p>
								<div class="botao-servicos">
									<div class="button icon_box_button trans_200">
										<a href="software.php?ser=Linx" class="trans_200">VER MAIS</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-lg-6 icon_box_col">
							<div class="icon_box_item">
								<figure>
									<img src="dev/img/icones/linx.png" alt="Linx">
								</figure>
								<h2>Top</h2>
								<p class="text-justify p06">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
								<div class="botao-servicos">
									<div class="button icon_box_button trans_200">
										<a href="software.php?ser=Top" class="trans_200">VER MAIS</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="device pt-100 pb-100">
		
		<div class="container_megasult">
			<div class="row">
				<div class="w-100">
					<div class="device_content" id="consultoria1">
						<h1 class="text-center">Consultoria</h1>
						<p class="text-center">Especialistas em estratégia e gestão para deixar sua empresa maior, mais forte e melhor.
						Oferecemos inteligência para empresas que buscam soluções e melhores resultados.</p>
					</div>
				</div>
				<div class="col-lg-6 col-xl-3 icon_box_col">
					<div class="icon_box_item">
						<figure>
							<img src="dev/img/icones/patentes.png" alt="Linx">
						</figure>
						<h2>Consultoria de Gestão Empresarial</h2>
						<p class="text-justify">Consiste no levantamento, tabulação e apuração dos fatos ocorridos na empresa, transformando-os em demonstrativos gerenciais.</p>
						<div class="botao-servicos">
							<div class="button icon_box_button trans_200">
								<a href="consultoria.php?con=gestao" class="trans_200">VER MAIS</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-xl-3 icon_box_col">
					<div class="icon_box_item">
						<figure>
							<img src="dev/img/icones/patentes.png" alt="Linx">
						</figure>
						<h2>Consultoria<br> Tributária</h2>
						<p class="text-justify">Consiste no levantamento, tabulação e apuração dos fatos ocorridos na empresa, transformando-os em demonstrativos gerenciais.</p>
						<div class="botao-servicos">
							<div class="button icon_box_button trans_200">
								<a href="consultoria.php?con=societaria" class="trans_200">VER MAIS</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-xl-3 icon_box_col">
					<div class="icon_box_item">
						<figure>
							<img src="dev/img/icones/patentes.png" alt="Linx">
						</figure>
						<h2>Consultoria<br> Societária</h2>
						<p class="text-justify">Consiste no levantamento, tabulação e apuração dos fatos ocorridos na empresa, transformando-os em demonstrativos gerenciais.</p>
						<div class="botao-servicos">
							<div class="button icon_box_button trans_200">
								<a href="consultoria.php?con=tributaria" class="trans_200">VER MAIS</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-xl-3 icon_box_col">
					<div class="device_content" id="consultoria2">
						<h1 class="text-right">Consultoria</h1>
						<p class="text-right">Especialistas em estratégia e gestão para deixar sua empresa maior, mais forte e melhor.
						Oferecemos inteligência para empresas que buscam soluções e melhores resultados.</p>
					</div>
				</div>
			</div>
		</div>

	</div>

	<!--<div class="cta">
		<div class="cta_background"></div>
		
		<div class="container">
			<div class="row">
				<div class="col-lg-12 order-lg-1 order-2">
					<div class="cta_content">
						<h1>Atuação no Paraná</h1>
						<?php
						$segundos_hoje = strtotime(date('Y-m-d'));

						$segundos_aniversario = strtotime(date('Y') . '-' . '09-'. '19');                     
						if($segundos_hoje >= $segundos_aniversario)
						{
						   $idade = date('Y') - '1997';
						}
						else
						{
						   $idade = date('Y') - '1996';
						}

						?>
						<p>Experiência de mais de <?=$idade?> anos, otimizando os negócios<br> e aumentando a competitividade de nossos clientes.</p>
					</div>
				</div>
			</div>
		</div>
	</div>-->

	<div class="text_line">
		<div class="container">
			<div class="row alinhar-vertical pb-100 pt-100">

				<div class="col-lg-8">
					<div>
						<img src="dev/img/trabalhe.jpg" alt="">
					</div>
				</div>

				<div class="col-lg-4">
					<div>
						<h1>Trabalhe Conosco</h1>
						<p class="text-justify">Buscamos criar diferenciais aos clientes através da gestão estratégica, operacional e tecnológica, com foco nas melhores práticas corporativas. Venha fazer parte da nossa história, trabalhe conosco.</p>
						<div class="button text_line_button trans_200">
							<a href="contato.php?ser=Trabalhe%20Conosco" class="trans_200">Enviar Currículo</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php 
	include 'includes/unidades.php';
	include 'includes/footer.php';
	include 'includes/scripts.php';
	?>
</div>
</body>

</html>