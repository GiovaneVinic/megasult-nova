<?php 
include 'data/config.php';

$titulo = "Contabilidade";
$bg = "dev/img/bread_background2.jpg";
$ser = $_GET['ser'];

?>
<!DOCTYPE html>
<html lang="pt-BR">

<?php 
include 'includes/head.php'; 
?>
<body>

<div class="super_container">

<?php 
include 'includes/header.php'; 
?>
<?php 
include 'includes/breadcrumb.php';
?>
<div class="container">
	<nav class="software-menu">
	    <ul>
	    	<div class="row">
		    	<li class="<?= ($ser == 'Auditoria Interna e Externa') ? 'active':''; ?> col-lg-2 col-md-6 col-sm-6">
		       		<a href="contabilidade.php?ser=Auditoria%20Interna%20e%20Externa" title="Auditoria Interna e Externa">
		        		<figure class="justify-content-center d-flex">
							<img src="dev/img/icones/amplus.png" width="40" height="40" alt="Auditoria Interna e Externa">
						</figure>
		        		<p>Auditoria e Perícias</p>
		        	</a>
		        </li>
		        <li class="<?= ($ser == 'Controle de Ativos') ? 'active':''; ?> col-lg-2 col-md-6 col-sm-6">
		       		<a href="contabilidade.php?ser=Controle%20de%20Ativos" title="Controle de ativos imobilizados">
		        		<figure class="justify-content-center d-flex">
							<img src="dev/img/icones/amplus.png" width="40" height="40" alt="Controle de ativos imobilizados">
						</figure>
		        		<p>Controle de ativos imobilizados</p>
		        	</a>
		        </li>
		    	<li class="<?= ($ser == 'Constituição de Empresas') ? 'active':''; ?> col-lg-3 col-md-6 col-sm-6">
		       		<a href="contabilidade.php?ser=Constituição%20de%20Empresas" title="Constituição, alteração e legalização de empresas">
		        		<figure class="justify-content-center d-flex">
							<img src="dev/img/icones/amplus.png" width="40" height="40" alt="Constituição, alteração e legalização de empresas">
						</figure>
		        		<p>Constituição, alteração e legalização de empresas</p>
		        	</a>
		        </li>
		        <li class="<?= ($ser == 'Escrituração Fiscal') ? 'active':''; ?> col-lg-2 col-md-6 col-sm-6">
		       		<a href="contabilidade.php?ser=Escrituração%20Fiscal" title="Escrituração Fiscal">
		        		<figure class="justify-content-center d-flex">
							<img src="dev/img/icones/amplus.png" width="40" height="40" alt="Escrituração Fiscal">
						</figure>
		        		<p>Escrituração Fiscal</p>
		        	</a>
		        </li>
		        <li class="<?= ($ser == 'Folha de Pagamento') ? 'active':''; ?> col-lg-2 col-md-6 col-sm-6">
		       		<a href="contabilidade.php?ser=Folha%20de%20Pagamento" title="Folha de Pagamento">
		        		<figure class="justify-content-center d-flex">
							<img src="dev/img/icones/amplus.png" width="40" height="40" alt="Folha de Pagamento">
						</figure>
		        		<p>Folha de Pagamento</p>
		        	</a>
		        </li>
		    </div>    
	    </ul>
	</nav>
</div>	

<?php 
	if ($ser == 'Constituição de Empresas') {
		include 'includes/contabilidade/constituicao.php';
	}else if ($ser == 'Contabilidade Fiscal') {
		include 'includes/contabilidade/contabilidade_fiscal.php';
	}else if ($ser == 'Escrituração Fiscal') {
		include 'includes/contabilidade/escrituracao_fiscal.php';
	}else if ($ser == 'Controle de Ativos') {
		include 'includes/contabilidade/ativos.php';
	}else if ($ser == 'Folha de Pagamento') {
		include 'includes/contabilidade/pagamento.php';
	}else if ($ser == 'Auditoria Interna e Externa') {
		include 'includes/contabilidade/auditoria.php';
	}
?>

<?php 
	include 'includes/unidades.php';
	include 'includes/footer.php';
	include 'includes/scripts.php';
?>
</body>
</html>