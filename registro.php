<?php 
include 'data/config.php';

$titulo = "Registro de Marcas";
$bg = "dev/img/bread_background.jpg";
$ser = $_GET['ser'];
?>
<!DOCTYPE html>
<html lang="pt-BR">

<?php 
include 'includes/head.php'; 
?>
<body>

<div class="super_container">

<?php 
include 'includes/header.php'; 
?>
<?php 
include 'includes/breadcrumb.php';
?>
<div class="container">
	<nav class="software-menu">
	    <ul>
	    	<div class="row alinhar-vertical">
		    	<li class="<?= ($ser == 'Código de Barras') ? 'active':''; ?> col-lg-2 col-md-3 col-sm-6">
		        	<a href="registro.php?ser=Código%20de%20Barras" title="Código de Barras">
		        		<figure class="justify-content-center d-flex">
							<img src="dev/img/icones/linx.png" width="40" height="40" alt="Código de Barras">
						</figure>
		        		<p>Código de Barras</p>
		        	</a>
		        </li>
		        <li class="<?= ($ser == 'Direito Autoral') ? 'active':''; ?> col-lg-2 col-md-3 col-sm-6">
		        	<a href="registro.php?ser=Direito%20Autoral" title="Direito Autoral">
		        		<figure class="justify-content-center d-flex">
							<img src="dev/img/icones/linx.png" width="40" height="40" alt="Direito Autoral">
						</figure>
		        		<p>Direito Autoral</p>
		        	</a>
		        </li>
		        <li class="<?= ($ser == 'Registro de Marcas') ? 'active':''; ?> col-lg-2 col-md-3 col-sm-6">
		        	<a href="registro.php?ser=Registro%20de%20Marcas" title="Registro de Marcas">
		        		<figure class="justify-content-center d-flex">
							<img src="dev/img/icones/amplus.png" width="40" height="40" alt="Registro de Marcas">
						</figure>
		        		<p>Registro de Marcas</p>
		        	</a>
		        </li>
		        <li class="<?= ($ser == 'Registro de Patentes') ? 'active':''; ?> col-lg-2 col-md-3 col-sm-6">
		        	<a href="registro.php?ser=Registro%20de%20Patentes" title="Registro de Patentes">
		        		<figure class="justify-content-center d-flex">
							<img src="dev/img/icones/iponto.png" width="40" height="40" alt="Registro de Patentes">
						</figure>
		        		<p>Registro de Patentes</p>
		        	</a>
		        </li>
		        <li class="<?= ($ser == 'Registro de Software') ? 'active':''; ?> col-lg-2 col-md-3 col-sm-6">
		        	<a href="registro.php?ser=Registro%20de%20Software" title="Registro de Software">
		        		<figure class="justify-content-center d-flex">
							<img src="dev/img/icones/linx.png" width="40" height="40" alt="Registro de Software">
						</figure>
		        		<p>Registro de Software</p>
		        	</a>
		        </li>	
	        </div>        
	    </ul>
	</nav>
</div>
<?php 
	if ($ser == 'Registro de Marcas') {
		include 'includes/registro/registro_marcas.php';
	}else if ($ser == 'Registro de Patentes') {
		include 'includes/registro/registro_patentes.php';
	}else if ($ser == 'Registro de Software') {
		include 'includes/registro/registro_software.php';
	}else if ($ser == 'Código de Barras') {
		include 'includes/registro/codigo_barras.php';
	}else if ($ser == 'Direito Autoral') {
		include 'includes/registro/direito_autoral.php';
	}
?>

<section class="registro">
    <div class="container">
    	<div class="w-100">
			<div class="col text-center">
				<div class="section_title">
					<h1>Registro de Marcas e Patentes</h1>
					<h3>Consultoria em propriedade intelectual e industrial</h3>
				</div>
			</div>
		</div>
		<div class="row mb-60">
			<div class="texto-registro">
				<p>A Divisão de Marcas e Patentes da Megasult visa auxiliá-lo na proteção de um dos maiores e mais importantes patrimônios: as suas marcas e invenções. Através de parceria com a AGP Consultoria de Ponta Grossa-PR, a Megasult presta assessoria em propriedade intelectual, auxiliando em todo o processo de registro e acompanhamento de marcas, patentes, direito autoral, softwares e código de barras.</p>
				<br>
				<p>Focada na ética e transparência dos serviços, a Megasult realiza visitas presenciais e transmite o máximo de informações. Assim, o cliente tem condições de entender e decidir a viabilidade de uma nova ação, e com nossa assessoria escolher a melhor opção para um novo registro.</p>
			</div>
		</div>
		<div class="row etapas-registro">
			<div class="col-lg-4 registro-box">
				<div class="w-100 registro-img">
					<img src="dev/img/icones/shield.png" alt="Proteção">
				</div>
				<h3>Tipo de Proteção</h3><br>
				<p>Patente de Invenção – PI </p>
				<p>Patente de Modelo de Utilidade – MU </p>
				<p>Registro de Desenho Industrial – DI </p>
				<i class="fas fa-long-arrow-alt-right"></i>
			</div>
			<div class="col-lg-4 registro-box">
				<div class="w-100 registro-img">
					<img src="dev/img/icones/idea.png" alt="Conceito">
				</div>
				<h3>Conceito</h3><br>
				<p>Criação industrial “nova”</p>
				<p>Aperfeiçoamento industrial “novo”</p>
				<p>Forma plástica “design” novo</p>
				<i class="fas fa-long-arrow-alt-right"></i>
			</div>
			<div class="col-lg-4 registro-box">
				<div class="w-100 registro-img">
					<img src="dev/img/icones/calendario.png" alt="Validade">
				</div>
				<h3>Validade</h3><br>
				<p>20 Anos;</p>
				<p>15 Anos;</p>
				<p>10 anos renováveis por mais 15;<br>(de 5 em 5) = 25 anos</p>
			</div>
		</div>
    </div>
</section>

<?php 
	include 'includes/unidades.php';
	include 'includes/footer.php';
	include 'includes/scripts.php';
?>
<script>
	$(document).on('click', '.collap', function(){
		var $icone = $(this).children('i')

		if($icone.hasClass('rotate'))
		{
			$icone.removeClass('rotate');
		}
		else
		{
			$icone.addClass('rotate');	
		}
	});
</script>
</body>
</html>