<?php 
include 'data/config.php';

$titulo = "Contato";
$bg = "dev/img/bread_background.jpg";
$ser = $_GET['ser'];

?>
<!DOCTYPE html>
<html lang="pt-BR">

<?php 
include 'includes/head.php'; 
?>
<body>

<div class="super_container">

<?php 
include 'includes/header.php'; 
?>
<?php 
include 'includes/breadcrumb.php';
?>
<div class="container">
	<nav class="software-menu">
	    <ul>
	    	<div class="row alinhar-vertical">
		        <li class="<?= ($ser == 'Atendimento') ? 'active':''; ?> col-lg-6 col-md-6 col-sm-6">
		        	<a href="contato.php?ser=Atendimento" title="Atendimento">
		        		<figure class="justify-content-center d-flex">
							<img src="dev/img/icones/amplus.png" width="40" height="40" alt="Atendimento">
						</figure>
		        		<p>Atendimento</p>
		        	</a>
		        </li>
		        <li class="<?= ($ser == 'Trabalhe Conosco') ? 'active':''; ?> col-lg-6 col-md-6 col-sm-6">
		        	<a href="contato.php?ser=Trabalhe%20Conosco" title="Trabalhe Conosco">
		        		<figure class="justify-content-center d-flex">
							<img src="dev/img/icones/iponto.png" width="40" height="40" alt="Trabalhe Conosco">
						</figure>
		        		<p>Trabalhe Conosco</p>
		        	</a>
		        </li>
	        </div>	        
	    </ul>
	</nav>
</div>
<?php 
	if ($ser == 'Atendimento') {
		include 'includes/contato/atendimento.php';
	}else if ($ser == 'Trabalhe Conosco') {
		include 'includes/contato/trabalhe.php';
	}
?>

<?php include 'includes/sobre/trabalhe.php'; ?>
<?php 
	include 'includes/unidades.php';
	include 'includes/footer.php';
	include 'includes/scripts.php';
?>
</body>
</html>