<?php 
include 'data/config.php';

$titulo = "Sistemas";
$bg = "dev/img/bread_background.jpg";
$ser = $_GET['ser'];

?>
<!DOCTYPE html>
<html lang="pt-BR">

<?php 
include 'includes/head.php'; 
?>
<body>

<div class="super_container">

<?php 
include 'includes/header.php'; 
?>
<?php 
include 'includes/breadcrumb.php';
?>
<div class="container">
	<nav class="software-menu">
	    <ul>
	    	<div class="row alinhar-vertical">
		        <li class="<?= ($ser == 'Amplus') ? 'active':''; ?> col-lg-3 col-md-3 col-sm-6">
		        	<a href="software.php?ser=Amplus" title="Amplus">
		        		<figure class="justify-content-center d-flex">
							<img src="dev/img/icones/amplus.png" width="40" height="40" alt="Amplus">
						</figure>
		        		<p>Amplus</p>
		        	</a>
		        </li>
		        <li class="<?= ($ser == 'Controle de Ponto') ? 'active':''; ?> col-lg-3 col-md-3 col-sm-6">
		        	<a href="software.php?ser=Controle%20de%20Ponto" title="Controle de Ponto">
		        		<figure class="justify-content-center d-flex">
							<img src="dev/img/icones/iponto.png" width="40" height="40" alt="Controle de Ponto">
						</figure>
		        		<p>Controle de Ponto</p>
		        	</a>
		        </li>
		        <li class="<?= ($ser == 'Linx') ? 'active':''; ?> col-lg-3 col-md-3 col-sm-6">
		        	<a href="software.php?ser=Linx" title="Linx - Moda e Acessórios">
		        		<figure class="justify-content-center d-flex">
							<img src="dev/img/icones/linx.png" width="40" height="40" alt="Linx">
						</figure>
		        		<p>Linx - Moda e Acessórios</p>
		        	</a>
		        </li>
		        <li class="<?= ($ser == 'Top') ? 'active':''; ?> col-lg-3 col-md-3 col-sm-6">
		        	<a href="software.php?ser=Top" title="Top Contabilidade">
		        		<figure class="justify-content-center d-flex">
							<img src="dev/img/icones/linx.png" width="40" height="40" alt="Top Contabilidade">
						</figure>
		        		<p>Top Contabilidade</p>
		        	</a>
		        </li>
	        </div>
	    </ul>
	</nav>
</div>
<?php 
	if ($ser == 'Amplus') {
		include 'includes/software/amplus.php';
	}else if ($ser == 'Controle de Ponto') {
		include 'includes/software/iponto.php';
	}else if ($ser == 'Linx') {
		include 'includes/software/linx.php';
	}else if ($ser == 'Top') {
		include 'includes/software/top.php';
	}
?>

</div>
<?php 
	include 'includes/unidades.php';
	include 'includes/footer.php';
	include 'includes/scripts.php';
?>
<script>
	$(document).on('click', '.collap', function(){
		var $icone = $(this).children('i')

		if($icone.hasClass('rotate'))
		{
			$icone.removeClass('rotate');
		}
		else
		{
			$icone.addClass('rotate');	
		}
	});
</script>
</body>
</html>