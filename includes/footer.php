<!-- Footer -->

<footer class="footer">
	<div class="container">
		<div class="row alinhar-vertical">
			
			<div class="col-lg-4">

				<!-- Footer Intro -->
				<div class="footer_intro">

					<!-- Logo -->
					<div class="logo footer_logo">
						<a href="#"><img src="dev/img/logo_250.png" alt="Megasult"></a>
					</div>

					<p>Voltada às necessidades de sua empresa a Megasult é o resultado da união de profissionais experientes em consultoria, nas áreas de Contabilidade Gerencial, Economia, Administração, Informática e Propriedade Industrial.</p>
					
					<!-- Social -->
					<div class="footer_social">
						<ul>
							<li><a href="https://www.instagram.com/megasult_contabilidade_/" target="_blank"><i class="fab fa-instagram" ></i></a></li>
							<li><a href="https://facebook.com/Megasult" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="https://www.linkedin.com/company/megasult-megasult/" target="_blank"><i class="fab fa-linkedin-in" ></i></a></li>
						</ul>
					</div>
					
					

				</div>

			</div>

			<!-- Footer Menu -->
			<div class="col-lg-4">

				<div class="footer_col">
					<div class="footer_col_title">Menu</div>
					<ul>
						<li><a href="index.php">Home</a></li>
						<li><a href="sobre.php">Megasult</a></li>
						<li><a href="contato.php?ser=Trabalhe%20Conosco">Trabalhe Conosco</a></li>
						<li><a href="contato.php?ser=Atendimento">Contato</a></li>
					</ul>
				</div>

			</div>

			<!-- Footer Menu -->
			<div class="col-lg-4">

				<div class="footer_col">
					<div class="footer_col_title">Serviços</div>
					<ul>
						<li><a href="software.php?ser=Amplus">Sistemas</a></li>
						<li><a href="contabilidade.php?ser=Constituição%20de%20Empresas">Contabilidade</a></li>
						<li><a href="consultoria.php?con=gestao">Consultoria</a></li>
						<li><a href="registro.php">Registro de Marcas</a></li>
					</ul>
				</div>

			</div>
		</div>
	</div>
	<!-- Copyright -->
		<div class="footer_cr">Copyright &copy;<script>document.write(new Date().getFullYear());</script> Todos os direitos reservados | Desenvolvido por <a href="https://e-mid.com.br" target="_blank">E-MID</a></div>
</footer>