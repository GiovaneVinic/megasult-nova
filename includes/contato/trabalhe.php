<section class="trabalhe">
    <div class="container">
    	<div class="w-100">
			<div class="col text-center">
				<div class="section_title">
					<h1>Trabalhe Conosco</h1>
					<h3>Venha fazer parte da nossa história, trabalhe conosco.</h3>
				</div>
			</div>
		</div>
		<div class="row">
			<form class="col-lg-12">
			  	<div class="form-group col-lg-6">
			    	<label for="">Nome</label><input type="text" class="form-control" id="nome" >
			  	</div>
			  	<div class="form-group col-lg-6">
			    	<label for="">E-mail</label><input type="email" class="form-control" id="email">
			  	</div>
			  	<div class="form-group col-lg-6">
			    	<label for="">Telefone</label><input type="tel" class="form-control" id="telefone">
			  	</div>
			  	<div class="form-group col-lg-6">
			  		<div class="curriculo">
			    	<label for="">Currículo</label><p class="curriculop">Selecione seu currículo...</p><input type="file" class="form-control" id="curriculo">

			    	</div>
			  	</div>
			  	<div class="form-group col-lg-12">
			    	<textarea type="text" class="form-control" id="mensagem" rows="5" placeholder="Mensagem"></textarea>
			  	</div>
			  	<div class="botao-contato col-lg-12">
			  	<button type="submit">Enviar</button>
			  	</div>
			</form>
		</div>
	</div>
</section>