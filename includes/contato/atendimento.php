<section class="contato">
    <div class="container">
    	<div class="w-100">
			<div class="col text-center">
				<div class="section_title">
					<h1>Atendimento</h1>
					<h3>Entre em contato conosco. Iremos lhe responder o mais breve possível.</h3>
				</div>
			</div>
		</div>
		<div class="row">
			<form class="col-lg-12">
			  	<div class="form-group col-lg-6">
			    	<label for="">Nome</label><input type="text" class="form-control" id="nome">
			  	</div>
			  	<div class="form-group col-lg-6">
			    	<label for="">E-mail</label><input type="email" class="form-control" id="email">
			  	</div>
			  	<div class="form-group col-lg-6">
			    	<label for="">Telefone</label><input type="tel" class="form-control" id="telefone">
			  	</div>
			  	<div class="form-group col-lg-6">
			    	<label for="">Assunto</label><input type="text" class="form-control" id="assunto">
			  	</div>
			  	<div class="form-group col-lg-6">
			    	<label for="">Estado</label><input type="text" class="form-control" id="estado">
			  	</div>
			  	<div class="form-group col-lg-6">
			    	<label for="">Cidade</label><input type="text" class="form-control" id="cidade">
			  	</div>
			  	<div class="form-group col-lg-12">
			    	<textarea type="text" class="form-control" id="mensagem" rows="8" placeholder="Mensagem"></textarea>
			  	</div>
			  	<div class="botao-contato col-lg-12">
			  	<button type="submit">Enviar</button>
			  	</div>
			</form>
		</div>
	</div>
</section>