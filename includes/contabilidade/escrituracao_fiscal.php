<div class="icon_boxes">
	<div class="container">
		<div class="row alinhar-vertical">
			<div class="col-lg-12" id="texto-contabilidade">
				<div class="icon_box_paragraph text-center">
					<h2 class="text-center">Escrituração Fiscal</h2><br>
					<p class="text-justify">Escrituração fiscal, livros de entrada, saída, apuração de inventário. Análise e enquadramento tributário, cálculo e emissão de guias de impostos, apresentação de informações ao Fisco Federal, Estadual e Municipal.</p>
				</div>
			</div>
		</div>
	</div>
</div>