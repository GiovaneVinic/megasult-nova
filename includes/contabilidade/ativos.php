<div class="icon_boxes">
	<div class="container">
		<div class="row alinhar-vertical">
			<div class="col-lg-12" id="texto-contabilidade">
				<div class="icon_box_paragraph text-center">
					<h2 class="text-center">Controle de ativos imobilizados</h2><br>
					<p class="text-justify">Controle do patrimônio, bem como dos bens adquiridos e alienados pela empresa com cálculo da depreciação e apuração do ganho de capital.</p>
				</div>
			</div>
		</div>
	</div>
</div>