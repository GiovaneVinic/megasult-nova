<div class="icon_boxes">
	<div class="container">
		<div class="row alinhar-vertical">
			<div class="col-lg-12" id="texto-contabilidade">
				<div class="icon_box_paragraph text-center">
					<h2 class="text-center">Folha de Pagamento</h2><br>
					<p class="text-justify">Elaboração da Folha de Pagamento, registros admissionais, férias, rescisões, enquadramento dos funcionários conforme Convenção Coletiva de Trabalho da categoria, apuração de impostos e contribuições sociais e previdenciárias, declarações obrigatórias e demais exigências previstas na legislação.</p>
				</div>
			</div>
		</div>
	</div>
</div>