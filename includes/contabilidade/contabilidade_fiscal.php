<div class="icon_boxes">
	<div class="container">
		<div class="row alinhar-vertical">
			<div class="col-lg-12" id="texto-contabilidade">
				<div class="icon_box_paragraph text-center">
					<h2 class="text-center">Contabilidade Fiscal</h2><br>
					<p class="text-justify">Escrituração contábil, livros diário e razão, elaboração de declarações obrigatórias, da pessoa jurídica e pessoa física, junto a Receita Federal, Estadual e Municipal.</p>
				</div>
			</div>
		</div>
	</div>
</div>