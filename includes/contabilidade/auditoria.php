<div class="icon_boxes">
	<div class="container">
		<div class="row alinhar-vertical">
			<div class="col-lg-12" id="texto-contabilidade">
				<div class="icon_box_paragraph text-center">
					<h2 class="text-center">Auditoria e Perícias</h2><br>
					<p class="text-justify">Realização de trabalhos nas áreas financeira, contábil e trabalhista, bem como a terceirização na implantação e manutenção de auditoria interna, por meio da análise e do desenvolvimento de programas de controladoria e planejamento específico.</p>
				</div>
			</div>
		</div>
	</div>
</div>