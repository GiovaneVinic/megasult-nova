<div class="icon_boxes">
	<div class="container">
		<div class="row alinhar-vertical">
			<div class="col-lg-12" id="texto-contabilidade">
				<div class="icon_box_paragraph text-center">
					<h2 class="text-center">Constituição, alteração e legalização de empresas</h2><br>
					<p class="text-justify">Elaboração do contrato social de empresas Ltda, S/A e entidades sem fins lucrativos, alterações contratuais, distratos, certidões negativas e legalização de empresas perante órgãos competentes.</p>
				</div>
			</div>
		</div>
	</div>
</div>