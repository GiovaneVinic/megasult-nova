<div class="icon_boxes">
	<div class="container">
		<div class="row alinhar-vertical mb-30">
			<div class="w-100">
				<div class="col text-center">
					<div class="section_title pb-30">
						<h1>A Empresa</h1>
					</div>
				</div>
			</div>
			<div class="col-lg-4 empresa">
				<figure class="logo-empresa alinhar-vertical">
					<img src="dev/img/logo_250.png" alt="A Megasult" >
				</figure>
			</div>

			<div class="col-lg-8" id="texto-amplus">
				<div class="icon_box_paragraph text-justify">
					<h2>Fundada em 1996, a megasult oferece soluções que otimizam os negócios e aumentam a competitividade de seus clientes.</h2>
				</div>

			</div>
		</div>
		<div class="row text-justify">
			<p>A Megasult iniciou suas atividades em 1996 e, desde então, vem se solidificando no mercado devido sua visão inovadora, estratégica e tecnológica que objetiva trazer soluções completas aos seus clientes, considerando as peculiaridades de cada um. Contamos com um portfólio de clientes em diversos ramos de atividade, para os quais dispomos serviços com a qualidade, agilidade e segurança que são nossas características principais.
			<br><br>
 
			Voltada às necessidades de sua empresa a Megasult é o resultado da união de profissionais altamente capacitados que atuam diariamente para transmitir a tranquilidade necessária para que nossos clientes possam se concentrar unicamente nas tomadas de decisões a frente da empresa.<br><br>

			Nosso objetivo é oferecer aos clientes e parceiros soluções completas através dos serviços de que dispomos em Contabilidade Fiscal, Consultoria Gerencial e de Gestão, Consultoria Tributária e Societária, Sistema ERP e Marcas e Patentes.
			</p>
		</div>
	</div>
</div>
<div class="valores">
	<div class="container">
		<div class="row">
			<div class="col-lg-4">
				<div class="objetivos">
					<h3>MISSÃO</h3><br>
					<p>“Fornecer soluções, com agilidade, qualidade, segurança e confiabilidade, transformando informações em benefício de nossos clientes, parceiros e colaboradores. ”</p>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="objetivos">
					<h3>VISÃO</h3><br>
					<p>“Promover o desenvolvimento dos clientes, colaboradores e da sociedade, através de soluções completas e diferenciadas."</p>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="objetivos">
					<h3>VALORES</h3><br>
			    	<p> • Ética<br>
			            • Integridade<br>
				  	    • Comprometimento<br>
				  	    • Credibilidade<br>
				  	    • Transparência
					</p>
				</div>
			</div>
		</div>
	</div>	
</div>
<div class="icon_boxes" style="background: #FFF!important;">
	<div class="container">
		<div class="row alinhar-vertical">
			<div class="w-100">
				<div class="col text-center">
					<div class="section_title pb-30">
						<h1>Nosso Objetivo</h1>
					</div>
				</div>
			</div>
		</div>
		<div class="row text-justify">
			<p>“Criar diferenciais aos clientes através da gestão estratégica, operacional e tecnológica, com foco nas melhores práticas corporativas, para que as empresas se fortaleçam nos mercados em que atuam.”
			</p>
		</div>
	</div>
</div>