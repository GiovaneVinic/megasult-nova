<?php 
	if (isset($ser)) {
		$breadcrumb_titulo = $ser;
	}else{
		$breadcrumb_titulo = $titulo;
	}
?>
<div class="breadcrumb">
	<div class="breadcrumb_background_container prlx_parent">
		<div class="breadcrumb_background prlx" style="background-image:url(<?=$bg?>)"></div>
	</div>
	<div class="breadcrumb_title">
		<h2><?=$titulo?></h2>
		<div class="atual">
			<a href="index.php">Início</a><i class="fas fa-angle-right"></i><?=$breadcrumb_titulo?>
		</div>
		<div class="next_section_scroll">
			<div class="next_section nav_links" data-scroll-to=".service_boxes">
				<i class="fas fa-chevron-down trans_200"></i>
				<i class="fas fa-chevron-down trans_200"></i>
			</div>
		</div>
	</div>
</div>