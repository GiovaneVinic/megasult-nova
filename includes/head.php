<head>
<title>Megasult | Site</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Megasult">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="dev/css/bootstrap4/bootstrap.min.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="plugins/fontawesome-free-5.0.1/css/fontawesome-all.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
<link rel="stylesheet" type="text/css" href="plugins/slick-1.8.0/slick.css">
<link href="plugins/icon-font/styles.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="dev/css/main_styles.css?v=1">
<link rel="stylesheet" type="text/css" href="dev/css/responsive.css">
<link rel="stylesheet" type="text/css" href="dev/css/fadein.css">
        
<meta name="title" content="Voltada às necessidades de sua empresa a Megasult é o resultado da união de profissionais experientes em consultoria, nas áreas de Contabilidade Gerencial, Economia, Administração, Informática e Propriedade Industrial."> 

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            
<meta name="copyright" content="© 2019 E-MID Soluções Inteligentes" />

<meta name="author" content="E-MID Agência Digital (e-mid.com.br)">

<meta name="description" content="Voltada às necessidades de sua empresa a Megasult é o resultado da união de profissionais experientes em consultoria, nas áreas de Contabilidade Gerencial, Economia, Administração, Informática e Propriedade Industrial.">

<meta name="resource-type" content="Consultoria">

<meta http-equiv="Content-Language" content="pt-br">

<meta name="keywords" content="consultoria, megasult, contabilidade, economia, administração, informática,propriedade industrial, patentes">

<meta name="rating" content="general">

<meta name="distribution" content="Global">

<meta name="robots" content="index, follow, all">

<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">                                                   
                               
<!------------------------------------------------------------------------------------------------------------>

<meta property="og:locale" content="pt_BR">

<meta property="og:site_name" content="Megasult">

<meta property="og:url" content="https://megasult.com.br/">

<meta property="og:type" content="Consultoria">

<meta property="og:title" content="Megasult">

<meta property="og:og:email" content="contato@megasult.com.br">

<meta property="og:phone_number" content="(46) 3211-2800">

<meta property="og:image" content="dev/img/logo.png">

<meta property="og:description" content="Voltada às necessidades de sua empresa a Megasult é o resultado da união de profissionais experientes em consultoria, nas áreas de Contabilidade Gerencial, Economia, Administração, Informática e Propriedade Industrial. ">        

<meta property="og:street_address" content="Rua Octaviano Teixeira dos Santos, 1373">

<meta property="og:postal-code" content="85601-030">

<link rel="apple-touch-icon" sizes="57x57" href="dev/img/favicons/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="dev/img/favicons/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="dev/img/favicons/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="dev/img/favicons/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="dev/img/favicons/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="dev/img/favicons/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="dev/img/favicons/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="dev/img/favicons/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="dev/img/favicons/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="dev/img/favicons/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="dev/img/favicons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="dev/img/favicons/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="dev/img/favicons/favicon-16x16.png">
<link rel="manifest" href="dev/img/favicons/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="dev/img/favicons/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
</head>