<section class="gestao">
    <div class="container">
    	<div class="w-100">
			<div class="col text-center">
				<div class="section_title">
					<h1>Consultoria de Gestão Empresarial</h1>
				</div>
			</div>
		</div>
        <div class="row alinhar-vertical">

            <div class="col-lg-6">
                <div class="gestao-box">
                    <h2>Planejameto Estratégico</h2><br>
                    <p>Processo estruturado de planejamento, no qual são definidas as diretrizes de ações da empresa para os próximos anos. Trata-se de planejar as ações para atingir objetivos de acordo com a estratégia adotada.</p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="gestao-box">
                    <h2>Diagnóstico E Análise De Viabilidade</h2><br>
                    <p>Estudo da capacidade da empresa gerar lucros. Identificação do Ponto de Equilíbrio Econômico e Financeiro. Necessidade de Capital de Giro.</p>
                </div>
            </div>
        </div>

        <div class="row alinhar-vertical">
            <div class="col-lg-12">
                <div class="gestao-box">
                    <h2>Apuração, Análise De Balanço E Demonstrativo De Resultado</h2><br>
                    <p>Consiste no levantamento, tabulação e apuração dos fatos ocorridos na empresa, transformando-os em demonstrativos gerenciais e comparativos gráficos, os quais são analisados e apresentados visando a tomada de decisões. Os indicadores de resultados têm por objetivo visualizar impactos financeiros, contribuindo em decisões que favorecem resultados a organização. Os indicadores de desempenho têm por objetivo apontar as formas de acompanhar, comparar e analisar os indicadores de resultados, afim de identificar possíveis receitas e despesas.</p>
                </div>
            </div>
        </div>

        <div class="row alinhar-vertical">
            <div class="col-lg-6">
                <div class="gestao-box">
                    <h2>Remuneração</h2><br>
                    <p>Critérios para pagamento do salário, comissões, prêmios, programa de participação de resultados concessão de benefícios. Respaldado pela Legislação Trabalhista e Convenção Coletiva de Trabalho.</p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="gestao-box">
                    <h2>Markup</h2><br>
                    <p>Demonstrará formas para calcular o preço de venda de seus produtos e/ou serviços. Ao final o gestor poderá realizar simulações instantâneas de valores de vendas com descontos e, com isso, conhecer seus ganhos reais nas vendas. </p>
                </div>
            </div>

        </div>

        <div class="row alinhar-vertical">
            <div class="col-lg-12">
                <div class="gestao-box">
                    <h2>Análise E Estruturação De Controles Internos</h2><br>
                    <p>Avaliação do ambiente de controle da empresa, objetivando verificar a qualidade, confiabilidade da informação. Constitui ferramenta eficaz na condução dos negócios e na tomada de decisões. Gestão de compras indicará estratégias para o procedimento de compras, adequando a quantidade e prazo de pagamento. Gestão de estoque fundamentará diferentes maneiras de organizar e controlar estoque.</p>
                </div>
            </div>
        </div>

        <div class="row alinhar-vertical">

            <div class="col-lg-6">
                <div class="gestao-box">
                    <h2>Avaliação De Desempenho</h2><br>
                    <p>
                        Objetivos de avaliação de desempenho, fatores que influenciam o desempenho, ferramentas de avaliação, planejamento de processo de avaliação de desempenho: antes, durante e pós.
                    </p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="gestao-box">
                    <h2>Liderança E Desenvolvimento De Equipe</h2><br>
                    <p>Orientação para o desenvolvimento da liderança para melhoria das relações da equipe e de atitudes que orientem, envolvam e comprometam a sua equipe para o alcance dos resultados.</p>
                </div>
            </div>

        </div>

        <div class="row alinhar-vertical">
            <div class="col-lg-12">
                <div class="gestao-box">
                    <h2>Planejamento Econômico E Financeiro</h2><br>
                    <p>
                        Elaboração de orçamentos e previsões dos resultados da empresa, bem como o planejamento para crescimento estruturado da organização. Execução de projetos para buscar capitais a longo prazo, objetivando a obtenção de capital de giro, recursos para investimentos e melhoria do fluxo de caixa da empresa.
                    </p>
                </div>
            </div>
        </div>

        <div class="row alinhar-vertical">
            <div class="col-lg-6">
                <div class="gestao-box mh-auto">
                    <h2>Rotatividade</h2><br>
                    <p>
                        A rotatividade de pessoal e plano de ação para diminui-la.
                    </p>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="gestao-box mh-auto">
                    <h2>Análise De Valor Da Empresa </h2><br>
                    <p>Cálculo da valorização da empresa perante ao mercado. </p>
                </div>
            </div>
        </div>

    </div>
</section>