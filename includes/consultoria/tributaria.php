<section class="gestao">
    <div class="container">
    	<div class="w-100">
			<div class="col text-center">
				<div class="section_title">
					<h1>Consultoria Tributária</h1>
				</div>
			</div>
		</div>
        <div class="row alinhar-vertical">

            <div class="col-lg-6">
                <div class="gestao-box">
                    <h2>Planejamento Tributário</h2><br>
                    <p>Estudo tributário com o objetivo de identificar e sugerir procedimentos legais que visam a redução da carga tributária.</p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="gestao-box">
                    <h2>Consultoria Fiscal Para Empresas Exportadoras/ Importadoras</h2><br>
                    <p>Habilitação das empresas junto aos órgãos competentes para exportação/importação.
                    Apuração de custos gerenciais e Formação de preço de venda.
                    </p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="gestao-box">
                    <h2>Consultoria Fiscal</h2><br>
                    <p>Acompanhamentos registros contábeis e fiscais, auxiliamos na correta interpretação das regras fiscais, apuração dos tributos e aplicação do planejamento fiscal conforme legislação vigente.</p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="gestao-box">
                    <h2>Recuperação De Créditos Fiscais</h2><br>
                    <p>Consiste em identificar possíveis créditos tributários não aproveitados corretamente e proceder as retificações e o pedido de ressarcimento, restituição e/ou a compensação.</p>
                </div>
            </div>
        </div>
    </div>
</section>