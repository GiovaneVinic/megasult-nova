<section class="gestao">
    <div class="container">
    	<div class="w-100">
			<div class="col text-center">
				<div class="section_title">
					<h1>Consultoria Societária</h1>
				</div>
			</div>
		</div>
        <div class="row alinhar-vertical">

            <div class="col-lg-6">
                <div class="gestao-box">
                    <h2>Proteção Patrimonial</h2><br>
                    <p>Estudo e estruturação para destinação dos bens patrimoniais a serem registrados na pessoa física ou jurídica.</p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="gestao-box">
                    <h2>Planejamento Sucessório</h2><br>
                    <p>Organizar a sucessão, a transferência do patrimônio para os herdeiros, tendo como objetivo a continuidade da organização e redução de custos na fase de transição.
                    </p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="gestao-box">
                    <h2>Reorganização Societária</h2><br>
                    <p>Transformação de tipos jurídicos de sociedades, empresas Limitadas, Sociedade Anônima, Holding’s.</p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="gestao-box">
                    <h2>Aquisições, Fusões, Cisões E Incorporações</h2><br>
                    <p>Acompanhamento das negociações, elaboração da documentação necessária e os registros nos órgãos competentes.</p>
                </div>
            </div>
        </div>
    </div>
</section>