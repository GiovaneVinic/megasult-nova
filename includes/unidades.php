<section class="unidades">
	<div class="container">
		<div class="row alinhar-vertical">
			<div class="w-100">
				<div class="col text-center">
					<div class="section_title">
						<h1>Nossas Unidades</h1>
						<?php
						$segundos_hoje = strtotime(date('Y-m-d'));

						$segundos_aniversario = strtotime(date('Y') . '-' . '09-'. '19');                     
						if($segundos_hoje >= $segundos_aniversario)
						{
						   $idade = date('Y') - '1997';
						}
						else
						{
						   $idade = date('Y') - '1996';
						}

						?>
						<p>Experiência de mais de <?=$idade?> anos, otimizando os negócios<br> e aumentando a competitividade de nossos clientes.</p>
					</div>
				</div>
			</div>
			<?php
                $unidade = array(
                    array('titulo'=>'Francisco Beltrão - PR','texto'=>'Rua Octaviano Teixeira dos Santos, 1373<br>
						Centro - CEP 85601-030', 'telefone'=>'(46) 3211-2800'),
                    array('titulo'=>'Maringá - PR','texto'=>'Av Humaitá, 542 – Sala 21<br>
						Edificio Comercial Itapoá – CEP 87014-200', 'telefone'=>'(44) 3225-0869'),
                    array('titulo'=>'Curitiba - PR','texto'=>'Rua Buenos Aires, 457 - Sala 131<br>
						Batel - CEP 80250-070', 'telefone'=>'(41) 3018-5808'),
                    array('titulo'=>' Londrina - PR','texto'=>'Rua João Pessoa, 90 – Sala 06<br>
						Jardim Agari – CEP 86.020-220', 'telefone'=>'(43) 3027-1918 '),
                    array('titulo'=>'Marmeleiro - PR','texto'=>'Rua de Marmeleiro, 99<br>
					    – CEP 85615-000', 'telefone'=>'(99) 99999-9999'),
						
                );
                foreach ($unidade as $key => $value):
            ?>
			<div class="col-lg-2 col-md-6 col-sm-12 local">
				<h3><i class="fas fa-map-marker-alt" style="padding-right: 10px; padding-top: 1px;"></i><?=$value['titulo']?></h3>
				<p style="margin: 0;"><?=$value['texto']?></p>
				<p><!--<i class="fas fa-phone"></i>--><?=$value['telefone']?></p>
			</div>
			<?php endforeach; ?>
		</div>
	</div>
</section>