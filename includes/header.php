<?php 
$activePage = explode('.', basename($_SERVER['PHP_SELF'], ""));
$page = $activePage[0];
?>

	<!-- Header -->

	<header class="header d-flex flex-row justify-content-end align-items-center trans_200">

		<!-- Logo -->
		<div class="logo mr-auto">
			<a href="index.php" title="Megasult" id="logo-branco"><img src="dev/img/logo_250.png" alt="Megasult"></a>
			<a href="index.php" title="Megasult" id="logo-azul"><img src="dev/img/logo_250.png" alt="Megasult"></a>
		</div>

		<!-- Navigation -->
		<nav class="main_nav justify-self-end text-right">
			<ul>
				<li class="<?= ($page == 'sobre') ? 'active':''; ?>"><a href="sobre.php">A Megasult</a></li>
				<li class="nav-item dropdown dmenu <?= ($page == 'contabilidade') ? 'active':''; ?> ">
            		<a class="nav-link dropdown-toggle" href="#" id="navbardrop1" data-toggle="dropdown">Contabilidade</a>
            		<div class="dropdown-menu fadein">
            			<a class="dropdown-item" href="contabilidade.php?ser=Auditoria%20Interna%20e%20Externa"><img src="dev/img/icones/linx.png" alt="Auditoria" width="30" height="30">Auditoria e Perícias</a>
              			<a class="dropdown-item" href="contabilidade.php?ser=Constituição%20de%20Empresas"><img src="dev/img/icones/amplus.png" alt="Constituição de Empresas" width="30" height="30">Constituição de Empresas</a>
              			<a class="dropdown-item" href="contabilidade.php?ser=Controle%20de%20Ativos"><img src="dev/img/icones/linx.png" alt="Controle de Ativos" width="30" height="30">Controle de Ativos</a>
              			<a class="dropdown-item" href="contabilidade.php?ser=Escrituração%20Fiscal"><img src="dev/img/icones/linx.png" alt="Escrituração Fiscal" width="30" height="30">Escrituração Fiscal</a>
              			<a class="dropdown-item" href="contabilidade.php?ser=Folha%20de%20Pagamento"><img src="dev/img/icones/linx.png" alt="Folha de Pagamento" width="30" height="30">Folha de Pagamento</a>
            		</div>
          		</li>
          		<li class="nav-item dropdown dmenu <?= ($page == 'consultoria') ? 'active':''; ?> ">
            		<a class="nav-link dropdown-toggle" href="#" id="navbardrop2" data-toggle="dropdown">Consultoria</a>
            		<div class="dropdown-menu fadein">
              			<a class="dropdown-item" href="consultoria.php?con=gestao"><img src="dev/img/icones/amplus.png" alt="Consultoria Empresarial" width="30" height="30">Consultoria Empresarial</a>
              			<a class="dropdown-item" href="consultoria.php?con=societaria"><img src="dev/img/icones/linx.png" alt="Consultoria Societária" width="30" height="30">Consultoria Societária</a>
              			<a class="dropdown-item" href="consultoria.php?con=tributaria"><img src="dev/img/icones/iponto.png" alt="Consultoria Tributária" width="30" height="30">Consultoria Tributária</a>
            		</div>
          		</li>
				<li class="nav-item dropdown dmenu <?= ($page == 'software') ? 'active':''; ?> ">

            		<a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">Sistemas</a>
            		<div class="dropdown-menu fadein">
              			<a class="dropdown-item" href="software.php?ser=Amplus"><img src="dev/img/icones/amplus.png" alt="Amplus" width="30" height="30">Amplus</a>
              			<a class="dropdown-item" href="software.php?ser=Controle%20de%20Ponto"><img src="dev/img/icones/iponto.png" alt="iPonto" width="30" height="30">Controle de Ponto</a>
              			<a class="dropdown-item" href="software.php?ser=Linx"><img src="dev/img/icones/linx.png" alt="Linx" width="30" height="30">Linx</a>
              			<a class="dropdown-item" href="software.php?ser=Top"><img src="dev/img/icones/linx.png" alt="Top" width="30" height="30">Top Contabilidade</a>
            		</div>
          		</li>
				<li class="nav-item dropdown dmenu <?= ($page == 'registro') ? 'active':''; ?>">
            		<a class="nav-link dropdown-toggle" href="#" id="navbardrop3" data-toggle="dropdown">Registro de Marcas</a>
            		<div class="dropdown-menu fadein">
            			<a class="dropdown-item" href="registro.php?ser=Código%20de%20Barras"><img src="dev/img/icones/iponto.png" alt="Código de Barras" width="30" height="30">Código de Barras</a>
              			<a class="dropdown-item" href="registro.php?ser=Direito%20Autoral"><img src="dev/img/icones/iponto.png" alt="Direito Autoral" width="30" height="30">Direito Autoral</a>
              			<a class="dropdown-item" href="registro.php?ser=Registro%20de%20Marcas"><img src="dev/img/icones/amplus.png" alt="Registro de Marcas" width="30" height="30">Registro de Marcas</a>
              			<a class="dropdown-item" href="registro.php?ser=Registro%20de%20Patentes"><img src="dev/img/icones/iponto.png" alt="Registro de Patentes" width="30" height="30">Registro de Patentes</a>
              			<a class="dropdown-item" href="registro.php?ser=Registro%20de%20Software"><img src="dev/img/icones/iponto.png" alt="Registro de Software" width="30" height="30">Registro de Software</a>
            		</div>
          		</li>
				<li class="nav-item dropdown dmenu <?= ($page == 'contato') ? 'active':''; ?>">
            		<a class="nav-link dropdown-toggle" href="#" id="navbardrop4" data-toggle="dropdown">Contato</a>
            		<div class="dropdown-menu fadein">
              			<a class="dropdown-item" href="contato.php?ser=Atendimento"><img src="dev/img/icones/amplus.png" alt="Atendimento" width="30" height="30">Atendimento</a>
              			<a class="dropdown-item" href="contato.php?ser=Trabalhe%20Conosco"><img src="dev/img/icones/iponto.png" alt="Trabalhe Conosco" width="30" height="30">Trabalhe Conosco</a>
            		</div>
          		</li>
			</ul>
						
			<!-- Search -->
			<div class="search">
				<div class="search_content d-flex flex-column align-items-center justify-content-center">
					<div class="search_button d-flex flex-column align-items-center justify-content-center">
						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							width="18px" height="18px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">
							<g>
								<g>
									<path class="search_path" fill="#FFFFFF" d="M89.354,10.609c-14.144-14.146-37.157-14.146-51.301,0c-6.852,6.853-10.625,15.964-10.625,25.655
										c0,8.829,3.132,17.174,8.87,23.771l-4.32,4.321l-4.402-4.403c-0.482-0.482-1.137-0.754-1.819-0.754s-1.337,0.271-1.819,0.754
										L3.31,80.584c-2.148,2.147-3.331,5.004-3.331,8.042s1.183,5.895,3.331,8.042C5.457,98.817,8.313,100,11.35,100
										c3.038,0,5.894-1.184,8.041-3.331l20.627-20.631c0.482-0.482,0.753-1.137,0.753-1.819s-0.271-1.337-0.753-1.819l-4.403-4.403
										l4.322-4.322c6.795,5.902,15.28,8.855,23.766,8.855c9.289,0,18.579-3.537,25.65-10.61c6.852-6.853,10.625-15.963,10.625-25.654
										C99.979,26.573,96.205,17.462,89.354,10.609z M15.753,93.029c-1.176,1.177-2.739,1.824-4.403,1.824
										c-1.663,0-3.227-0.648-4.403-1.824c-1.176-1.176-1.823-2.74-1.823-4.403s0.647-3.228,1.824-4.403L18.458,72.71l8.805,8.807
										L15.753,93.029z M30.902,77.878l-8.805-8.807l3.659-3.66l8.806,8.808L30.902,77.878z M85.715,58.28
										c-12.137,12.14-31.886,12.14-44.023,0c-5.88-5.881-9.118-13.699-9.118-22.016c0-8.316,3.238-16.135,9.118-22.016
										c6.069-6.069,14.041-9.104,22.012-9.104c7.972,0,15.943,3.035,22.013,9.104c5.88,5.881,9.117,13.699,9.117,22.016
										S91.596,52.399,85.715,58.28z"></path>
								</g>
							</g>
							<g>
								<g>
									<path class="search_path" fill="#FFFFFF" d="M81.47,18.495c-9.797-9.798-25.736-9.798-35.533,0c-9.796,9.798-9.796,25.741,0,35.539
										c4.898,4.898,11.333,7.349,17.766,7.349c6.435,0,12.868-2.45,17.767-7.349l0,0C91.266,44.235,91.266,28.293,81.47,18.495z
										 M77.831,50.395c-7.79,7.791-20.466,7.791-28.256,0c-7.79-7.792-7.79-20.469,0-28.261c3.896-3.896,9.011-5.843,14.128-5.843
										c5.116,0,10.233,1.948,14.128,5.843C85.621,29.925,85.621,42.603,77.831,50.395z"></path>
								</g>
							</g>
							<g>
								<g>
									<path class="search_path" fill="#FFFFFF" d="M73.283,26.683c-5.282-5.283-13.877-5.283-19.16,0c-1.004,1.005-1.004,2.634,0,3.639
										c1.005,1.005,2.634,1.005,3.639,0c3.276-3.276,8.607-3.276,11.884,0c0.502,0.503,1.16,0.754,1.818,0.754
										c0.659,0,1.317-0.251,1.819-0.754C74.288,29.317,74.288,27.688,73.283,26.683z"></path>
								</g>
							</g>
						</svg>
					</div>

					<form id="search_form" class="search_form bez_1">
						<input type="search" class="search_input bez_1">
					</form>

				</div>
			</div>
		</nav>

		<!-- Hamburger -->
		<div class="hamburger_container bez_1">
			<i class="fas fa-bars trans_200"></i>
		</div>
		
	</header>

	<!-- Menu -->

	<div class="menu_container">
		<div class="menu menu_mm text-right">
			<div class="menu_close"><i class="far fa-times-circle trans_200"></i></div>
			<ul>
				<li class="menu-mm<?= ($page == 'sobre') ? 'active':''; ?>"><a href="sobre.php">A Megasult</a></li>
				<li class="menu-mm dropdown dmenu <?= ($page == 'contabilidade') ? 'active':''; ?> ">
            		<a class="nav-link dropdown-toggle" href="#" id="navbardrop1" data-toggle="dropdown">Contabilidade</a>
            		<div class="dropdown-menu fadein">
            			<a class="dropdown-item" href="contabilidade.php?ser=Auditoria%20Interna%20e%20Externa"><img src="dev/img/icones/linx.png" alt="Auditoria" width="30" height="30">Auditoria e Perícias</a>
              			<a class="dropdown-item" href="contabilidade.php?ser=Constituição%20de%20Empresas"><img src="dev/img/icones/amplus.png" alt="Constituição de Empresas" width="30" height="30">Constituição de Empresas</a>
              			<a class="dropdown-item" href="contabilidade.php?ser=Controle%20de%20Ativos"><img src="dev/img/icones/linx.png" alt="Controle de Ativos" width="30" height="30">Controle de Ativos</a>
              			<a class="dropdown-item" href="contabilidade.php?ser=Escrituração%20Fiscal"><img src="dev/img/icones/linx.png" alt="Escrituração Fiscal" width="30" height="30">Escrituração Fiscal</a>
              			<a class="dropdown-item" href="contabilidade.php?ser=Folha%20de%20Pagamento"><img src="dev/img/icones/linx.png" alt="Folha de Pagamento" width="30" height="30">Folha de Pagamento</a>
            		</div>
          		</li>
          		<li class="menu-mm dropdown dmenu <?= ($page == 'consultoria') ? 'active':''; ?> ">
            		<a class="nav-link dropdown-toggle" href="#" id="navbardrop2" data-toggle="dropdown">Consultoria</a>
            		<div class="dropdown-menu fadein">
              			<a class="dropdown-item" href="consultoria.php?con=gestao"><img src="dev/img/icones/amplus.png" alt="Consultoria Empresarial" width="30" height="30">Consultoria Empresarial</a>
              			<a class="dropdown-item" href="consultoria.php?con=societaria"><img src="dev/img/icones/linx.png" alt="Consultoria Societária" width="30" height="30">Consultoria Societária</a>
              			<a class="dropdown-item" href="consultoria.php?con=tributaria"><img src="dev/img/icones/iponto.png" alt="Consultoria Tributária" width="30" height="30">Consultoria Tributária</a>
            		</div>
          		</li>
				<li class="menu-mm dropdown dmenu <?= ($page == 'software') ? 'active':''; ?> ">

            		<a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">Sistemas</a>
            		<div class="dropdown-menu fadein">
              			<a class="dropdown-item" href="software.php?ser=Amplus"><img src="dev/img/icones/amplus.png" alt="Amplus" width="30" height="30">Amplus</a>
              			<a class="dropdown-item" href="software.php?ser=Controle%20de%20Ponto"><img src="dev/img/icones/iponto.png" alt="iPonto" width="30" height="30">Controle de Ponto</a>
              			<a class="dropdown-item" href="software.php?ser=Linx"><img src="dev/img/icones/linx.png" alt="Linx" width="30" height="30">Linx</a>
              			<a class="dropdown-item" href="software.php?ser=Top"><img src="dev/img/icones/linx.png" alt="Top" width="30" height="30">Top Contabilidade</a>
            		</div>
          		</li>
				<li class="menu-mm dropdown dmenu <?= ($page == 'registro') ? 'active':''; ?>">
            		<a class="nav-link dropdown-toggle" href="#" id="navbardrop3" data-toggle="dropdown">Registro de Marcas</a>
            		<div class="dropdown-menu fadein">
            			<a class="dropdown-item" href="registro.php?ser=Código%20de%20Barras"><img src="dev/img/icones/iponto.png" alt="Código de Barras" width="30" height="30">Código de Barras</a>
              			<a class="dropdown-item" href="registro.php?ser=Direito%20Autoral"><img src="dev/img/icones/iponto.png" alt="Direito Autoral" width="30" height="30">Direito Autoral</a>
              			<a class="dropdown-item" href="registro.php?ser=Registro%20de%20Marcas"><img src="dev/img/icones/amplus.png" alt="Registro de Marcas" width="30" height="30">Registro de Marcas</a>
              			<a class="dropdown-item" href="registro.php?ser=Registro%20de%20Patentes"><img src="dev/img/icones/iponto.png" alt="Registro de Patentes" width="30" height="30">Registro de Patentes</a>
              			<a class="dropdown-item" href="registro.php?ser=Registro%20de%20Software"><img src="dev/img/icones/iponto.png" alt="Registro de Software" width="30" height="30">Registro de Software</a>
            		</div>
          		</li>
				<li class="menu-mm dropdown dmenu <?= ($page == 'contato') ? 'active':''; ?>">
            		<a class="nav-link dropdown-toggle" href="#" id="navbardrop4" data-toggle="dropdown">Contato</a>
            		<div class="dropdown-menu fadein">
              			<a class="dropdown-item" href="contato.php?ser=Atendimento"><img src="dev/img/icones/amplus.png" alt="Atendimento" width="30" height="30">Atendimento</a>
              			<a class="dropdown-item" href="contato.php?ser=Trabalhe%20Conosco"><img src="dev/img/icones/iponto.png" alt="Trabalhe Conosco" width="30" height="30">Trabalhe Conosco</a>
            		</div>
          		</li>
			</ul>
		</div>
	</div>