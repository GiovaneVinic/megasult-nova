<div class="icon_boxes">
	<?php include 'includes/software/segmentos.php' ?>
	<div class="container">
		<div class="row alinhar-vertical">
			<div class="w-100">
				<div class="col text-center">
					<div class="section_title">
						<h1>Linx - Moda e Acessórios</h1>
					</div>
				</div>
			</div>
			<div class="col-lg-4 segmentos">
				<h2>Características</h2>
				<ul>
					<li><i class="fas fa-angle-right"></i> Experiência do Cliente</li>
					<li><i class="fas fa-angle-right"></i> Aumento de Rentabilidade</li>
					<li><i class="fas fa-angle-right"></i> Integração Multicanal</li>
					<li><i class="fas fa-angle-right"></i> Gestão de Recebimentos</li>
					<li><i class="fas fa-angle-right"></i> Gestão de Franquias</li>
					<li><i class="fas fa-angle-right"></i> Vender Mais</li>
					<li><i class="fas fa-angle-right"></i> Gestão de Produção</li>
				</ul>
			</div>

			<div class="col-lg-8" id="texto-amplus">

				<!-- Icon Box Item -->
				<div class="icon_box_paragraph">
					<h2>Experiência do cliente</h2><br>
					<p>Conheça seu consumidor e ofereça a ele a melhor experiência de compra. Nossas tecnologias processam informações através de inteligência artificial e Data Science para personalizar as ofertas e oferecer o produto ideal para a pessoa certa. Mapeamos o comportamento dos seus consumidores na loja física e no e-commerce, ajudando você a criar momentos únicos a cada etapa da jornada de compra.</p>
				</div>

			</div>
		</div>
	</div>
</div>
<div class="linx">
	<div class="container">
		<div class="row mb-80">
			<div class="col-lg-6 conteudo-linx">
				<h2 class="text-center">Integração multicanal</h2><br>
				<p class="text-justify">Permitir que seu cliente escolha a melhor maneira de comprar é essencial para promover o melhor relacionamento com a sua marca. Proporcione essa experiência para o seu cliente, no ambiente online ou offline. Ganha ele e ganha você, que integra as operações de lojas, franquias e centro de distribuição para reduzir o déficit de estoque, gerar mais tráfego e aumentar as vendas.</p>
			</div>
			<div class="col-lg-6 conteudo-linx">
				<h2 class="text-center">Gestão de recebimentos</h2><br>
				<p class="text-justify">Quer garantir todas as vendas da sua loja? Então você está no lugar certo. Com nossas soluções você recebe pagamentos de inúmeras bandeiras de cartões de crédito, débito ou voucher, tanto online quanto offline, com total controle e segurança. Tudo isso de forma customizada para você ter acesso às funcionalidades que melhor atendem às estratégias do seu negócio.</p>
			</div>
		</div>
		<div class="row mb-80 alinhar-vertical">
			<div class="col-lg-4 icone-texto">
				<img src="dev/img/online-shop.png" alt="Linx - Moda e Acessórios">
			</div>
			<div class="col-lg-8">
				<h2>Ofereça a melhor experiência de compra</h2><br>
				<p class="text-justify">Nossos sistemas para lojas de roupas e acessórios, possibilitam a compreensão da jornada do consumidor, buscando aperfeiçoar as estratégias do seu negócio. Dessa maneira, você garante um atendimento eficiente e facilita para que cada perfil de consumo tenha acesso às ofertas ideais, no momento certo. É controle total para você: da fábrica ao pós-venda.</p>
			</div>
		</div>
		<div class="row mb-80">
			<div class="col-lg-6 conteudo-linx">
				<h2 class="text-center">Gestão de franquias</h2><br>
				<p class="text-justify">Centralize a gestão de sua rede ou franquia. Aqui, você tem ferramentas para integrar pedidos de compra, fazer a transferência de estoque, controlar ações promocionais das unidades e padronizar a administração de produtos. É tudo em tempo real e você ainda pode acompanhar o desempenho por loja ou região, filtrando por mercadoria, vendedor, marca ou até linha de produtos.</p>
			</div>
			<div class="col-lg-6 conteudo-linx">
				<h2 class="text-center">Vender mais</h2><br>
				<p class="text-justify">A Linx tem sistemas feitos sob medida para o segmento de moda e acessórios. Conte com nossas ferramentas de atração e engajamento para transformar a experiência dos consumidores, ficar por dentro das tendências e oferecer promoções exclusivas baseadas no perfil de compra dos clientes. Você ainda gerencia estoques e integra loja física e virtual, permitindo que o cliente compre da forma que preferir. É tudo que você precisa para vender ainda mais.</p>
			</div>
		</div>
		<div class="row mb-60 justify-content-center d-flex">
			<div class="col-lg-12">
				<h2 class="text-center">Gestão de Produção</h2><br>
				<p class="text-justify">Ter o controle de todo o processo produtivo da sua indústria é o desejo de todo empreendedor. E é com o objetivo de atender essa necessidade que o módulo de produção Linx ERP fornece ferramentas que auxiliam a tomada de decisão, além de garantir a produtividade, reduzir custos de produção e certificar a qualidade do produto acabado, no momento certo.</p>
			</div>
		</div>
	</div>
</div>