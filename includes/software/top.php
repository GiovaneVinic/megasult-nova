<div class="icon_boxes">
	<?php include 'includes/software/segmentos.php' ?>
	<div class="container">
		<div class="row">
			<div class="w-100">
				<div class="col text-center">
					<div class="section_title">
						<h1>Contabilidade</h1>
					</div>
				</div>
			</div>
			<div class="col-lg-12 lista-iponto">
				<ul>
					<li><i class="fas fa-angle-right"></i> Plano de contas flexível, com opção de centro de custo e modelos por atividade.</li>
					<li><i class="fas fa-angle-right"></i> Análise Econômico/Financeiras.</li>
					<li><i class="fas fa-angle-right"></i> Históricos padrões e flexíveis.</li>
					<li><i class="fas fa-angle-right"></i> Lançamentos de partidas dobradas, simples e futuras, matriz e/ou unidades de negócio.</li>
					<li><i class="fas fa-angle-right"></i> Plano de contas com grau hierárquico sem limites.</li>
					<li><i class="fas fa-angle-right"></i> Parametrização de máscara de conta livre quanto ao nº de dígitos.</li>
					<li><i class="fas fa-angle-right"></i> Demonstrações contábeis e gerenciais em formato caracter e gráfico, impressão em sequência e em outras moedas ou índices.</li>
					<li><i class="fas fa-angle-right"></i> Planejamento e controle orçamentário.</li>
					<li><i class="fas fa-angle-right"></i> Consolidação de empresas e filiais.</li>
					<li><i class="fas fa-angle-right"></i> Rateios.</li>
					<li><i class="fas fa-angle-right"></i> Encerramento automático das contas de resultado, em períodos definidos pelo usuário definidos pelo usuário e em aberto.</li>
					<li><i class="fas fa-angle-right"></i> Reabertura automática dos saldos no início do exercício.</li>
					<li><i class="fas fa-angle-right"></i> Conciliação bancária e contábil.</li>
					<li><i class="fas fa-angle-right"></i> Geração de arquivos digitais: SPED Contábil, Receita Federal e Previdenciária.</li>
					<li><i class="fas fa-angle-right"></i> Movimentação contábil independente do encerramento do exercício anterior.</li>
					<li><i class="fas fa-angle-right"></i> Compartilhamento de cadastros entre empresas.</li>
					<li><i class="fas fa-angle-right"></i> Integração contábil com os demais módulos Exactus de contabilidade e de gestão empresarial.</li>
					<li><i class="fas fa-angle-right"></i> DLPA, DOAR, DVA, DMPL, DFC e outros.</li>
					<li><i class="fas fa-angle-right"></i> Exporta dados para DIPJ.</li>
					<li><i class="fas fa-angle-right"></i> Outros dispositivos específicos.</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="accord">
	<div class="container">
		<div class="w-100">
				<div class="col text-center">
					<div class="section_title">
						<h1>Top Contabilidade</h1>
					</div>
				</div>
			</div>
		<?php
			$i = 0;
            $accordeon = array(
                array('titulo'=>'Exactus Lalur','texto'=>'✓ Realiza cálculo do Imposto de Renda e Contribuição Social;<br>✓ Controla a parte B do LALUR;<br>✓ Emite o livro LALUR e o termo de abertura/encerramento;<br>✓ Emite o Registro dos Ajustes do Lucro Líquido.'),
                array('titulo'=>'Folha de Pagamento','texto'=>'✓ Calcula folha mensal, adiantamento, complementar e 13º Salário;<br>✓ Realiza cálculo específico para multicontratos e centros de custos;<br>✓ Realiza o cálculo de férias individuais, coletivas, em lote, programadas e diferença de férias;
				<br>✓ Realiza cálculo de rescisões e rescisões complementares;<br>✓ Emite GPS, GRCSU, e DARF do IRRF e PIS;<br>✓ Gera informações para o SEFIP, GRRF, CAGED, RAIS, DIRF, MAND, TCE e COORDIGUALIDADE;<br>✓ Gera provisão de férias e 13º Salário;<br>✓ Possibilita a integração da provisão e movimentações com o módulo contábil;<br>✓ Possibilita a integração de informações do ponto eletrônico;<br>✓ Armazena e emite informações do PPP;<br>✓ Possui cadastro e gera arquivos para bancos;<br>✓ Possui cadastro e gera arquivos de pedido do vale – transporte e alimentação/refeição;<br>✓ Armazena índices e faz ajustes monetários;<br>✓ Possui cadastro e controle para lançamento de eventos parcelados;<br>✓ Possui cadastro e realiza a apuração de participações nos lucros;<br>✓ Possui mecanismo de transferência de empregados entre empresas;<br>✓ Possui controle de alterações salariais;<br>✓ Faz a emissão do mapa de troco;<br>✓ Faz a emissão de cheques;<br>✓ Possui diversos relatórios cadastrais e de movimentação;<br>✓ Possui editor de relatórios em página cheia, etiquetas e linha/coluna;<br>✓ Faz extração de dados em diversos formatos.'),
                array('titulo'=>'Controle Patrimonial','texto'=>'✓ Controle dos itens patrimoniais, organizados por conta, tipo, local físico, seguradora, fornecedor, com armazenamento da imagem dos itens;<br>✓ Cálculo de depreciação mensal/anual, com percentual de depreciação por conta ou item, permitindo suspensão/aceleração;<br>✓ Controla ICMS na aquisição de imobilizado;<br>✓ Cálculo retroativo na implantação de itens;<br>✓ Rateio de valores acumulados na implantação de itens;<br>✓ Baixa total ou parcial dos itens, individual ou em grupo, por quantidade ou valor, com apuração de ganhos ou perdas na baixa por venda;<br>✓ Transferência ou parcial dos itens, por quantidade de valor;<br>✓ Integração contábil, permitindo a contabilização por centro de custos;<br>✓ Relação analítica ou sintética dos itens adquiridos;<br>✓ Relação dos itens baixados e transferidos;<br>✓ Relatório razão auxiliar mensal e acumulado;<br>✓ Relatório resumo da depreciação/amortização/exaustão;<br>✓ Relatório de situação dos itens e saldos do imobilizado;<br>✓ Relatório termo de responsabilidade;<br>✓ Impressão dos relatórios em sequência pré-definida pelo usuário;
					<br>✓ Exportação de saldos para planilha do Excel e arquivo texto.'),
                 array('titulo'=>'Escrita Fiscal','texto'=>'✓ Integração contábil on-line ou através da disponibilização de períodos previamente definidos pelo usuário com o sistema de contabilidade Exactus, ou ainda através da exportação dos lançamentos contábeis emarquivo para posterior importação no sistema de contabilidade;<br>✓ Controle de retenções de imposto e contribuições de ISS, INSS, PIS, COFINS e contribuição social;<br>✓ Faz a emissão dos guias federais e estaduais com o devido controle e contabilização do pagamento;<br>✓ Faz a importação de dados através dos dados gerados no arquivo do Sintegra e/ou arquivo do SPED Fiscal, ou ainda, de arquivos de texto com layout parametrizável pelo usuário;<br>✓ Gera informativos do ICMS (Mensal e Anual) dos seguintes Estados: RS, SC, PR, MS, MT, GO, DF, SP, RJ, MG, ES, BA, PE, PB, CE, RN:<br>✓ Gera declarações municipais das capitais e de vários outros municípios;<br>✓ Gera o Sintegra, Dacon, DCTF, entre outros;<br>✓ Gera o EFD (Escrituração Fiscal Digital – Projeto SPED);<br>✓ Faz o preenchimento automático do PGDAS diretamente do portal no SIMPLES Nacional;<br>✓ Faz a emissão e controle do C.I.A.P. (Controle do ICMS do Ativo Permanente) modelos C e D;<br>✓ Faz o controle e baixa de duplicatas de fornecedores e clientes por vencimento ou saldo;<br>✓ Com módulo I pode gerar automaticamente o DCTF e DIRF.'),
            );
            foreach ($accordeon as $key => $value):
        ?>
		<div class="collap" id="collap" data-toggle="collapse" data-target="#accord<?=$i?>">
			<i class="fas fa-angle-right icone <?= $i == 0 ? "rotate" : "" ?>"></i><a class="accord-amplus" ><?=$value['titulo']?></a>
			<div id="accord<?=$i?>" class="collapse <?= $i == 0 ? "show" : "" ?>">
				<?=$value['texto']?>
			</div>
		</div>
		<?php $i++; endforeach;?>
	</div>
</div>
<section class="top">
    <div class="container">
    	<div class="w-100">
			<div class="col text-center">
				<div class="section_title">
					<h1>Gestão da empresa de contabilidade</h1>
				</div>
			</div>
		</div>
        <div class="row alinhar-vertical">

            <div class="col-lg-6">
                <div class="top-box">
                    <h2>Gerenciador de Escritório</h2><br>
                    <p>Interligação com a Contabilidade, Folha, Escrita e Controle Patrimonial.<br>DCFT automático.</p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="top-box">
                    <h2>Exactus Protocolo</h2><br>
                    <p>Possui baixa parcial ou total do protocolo.<br>Controla protocolos pendentes, baixados ou em aberto.
                    </p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="top-box">
                    <h2>Exactus Faturamento</h2><br>
                    <p>Emissão de notas de débito, boleto bancário, conta corrente do cliente, relatórios faturamento em aberto e baixados.</p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="top-box">
                    <h2>Controle de Tributos</h2><br>
                    <p>Agenda de obrigações, controle de entrega e retorno do protocolo.</p>
                </div>
            </div>
        </div>
        <div class="w-100">
			<div class="col text-center">
				<div class="section_title" id="vantagem">
					<h1>Vantagens</h1>
				</div>
			</div>
		</div>
		<div class="row alinhar-vertical">
			<div class="col-lg-6">
                <div class="top-box">
                    <h2>Exactus Administrar: Módulo 1</h2><br>
                    <p>Emite relatório com o tempo de trabalho de cada funcionário, com o tempo de uso de cada módulo do sistema, custo/benefício, demonstrando o tempo de trabalho com cada cliente conforme quantidade de lançamentos com Folha, Escrita e Contabilidade. Controla o total lançamento contábeis e fiscais por cada funcionário paracada cliente.</p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="top-box">
                    <h2>Agilidade e rapidez nos processos e na tomada de decisões</h2><br>
                    <p>Empresas que conseguem fechar suas escriturações mais rapidamente através de dados integrados e preciosos, obtêm análises de dados integrados e preciosos, obtêm análises e relatórios avançados gerando informações para tomada de decisões em menos tempo.</p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="top-box">
                    <h2>Otimização na rotina dos funcionários</h2><br>
                    <p>Com o nosso software de gestão, os colaboradores eliminam rotinas manuais, podendo assim, utilizar seu potencial em outras áreas estratégicas valorizando o tempo de trabalho em equipe.
                    </p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="top-box">
                    <h2>Ganho de mercado – vantagem competitiva</h2><br>
                    <p>Com mais agilidade, rapidez e eficiência, sua empresa diminui o tempo gasto com os exercícios de rotina, o que facilita o trabalho na execução de todos os processos. Ou seja, ganhe tempo para atendermais clientes.</p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="top-box">
                    <h2>Eficiência e ganho de produtividade</h2><br>
                    <p>Diminuição de custos ociosos e aumento da rentabilidade.</p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="top-box">
                    <h2>Atendimento/suporte presencial e constante</h2><br>
                    <p>Visitas presenciais, virtuais (Chat, MSN).<br> Tecnologia de EAD – Ensino à distância.</p>
                </div>
            </div>
        </div>
    </div>
</section>