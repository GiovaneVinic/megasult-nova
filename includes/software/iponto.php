<div class="icon_boxes">
	<?php include 'includes/software/segmentos.php' ?>
	<div class="container">
		<div class="row alinhar-vertical">
			<div class="w-100">
				<div class="col text-center">
					<div class="section_title">
						<h1>iPonto</h1>
					</div>
				</div>
			</div>
			<div class="col-lg-8 segmentos">
				<h2>Características</h2>
				<ul>
					<li><i class="fas fa-angle-right"></i> Sua interface amigável e intuitiva oferece um baixo tempo de aprendizado.</li>
					<li><i class="fas fa-angle-right"></i> Recursos práticos e flexíveis que dinamizam o tratamento do ponto.</li>
					<li><i class="fas fa-angle-right"></i> Relatórios completos que oferecem informações relevantes para a tomada de decisão.</li>
					<li><i class="fas fa-angle-right"></i> Alertas de CLT que auxiliam nos controles de prazos e desvios de regras.</li>
					<li><i class="fas fa-angle-right"></i> Automação das rotinas operacionais e gerenciais de sua empresa.</li>
					<li><i class="fas fa-angle-right"></i> Integração com os principais fabricantes de Rep – Registradores Eletrônicos de Ponto (TEM e Inmetro) e equipamentos 373.</li>
				</ul>
			</div>

			<div class="col-lg-4" id="texto-amplus">

				<!-- Icon Box Item -->
				<div class="icon_box_paragraph">
					<h3>O iPonto é um sistema completo destinado a gestão e controle de frequência dos colaboradores de sua organização.</h3><br>
					<p>Com características singulares, nossa ferramenta foi desenvolvida utilizando tecnologia de ponta, focada na segurança de armazenamento dos dados, na rapidez e na qualidade de seus processos.</p>
				</div>

			</div>
		</div>
	</div>
</div>

<div class="modulo-web">
	<div class="container">
		<div class="row alinhar-vertical">
			<div class="col-lg-4 icone-texto">
			 	<img src="dev/img/analytics.png" alt="iPonto Módulo Web">
			</div>
			<div class="col-lg-8 iponto-texto">
				<h2>iPonto Módulo Web</h2><br>
				<p>Permite que os funcionários visualizem e interajam com seu cartão ponto, e que os responsáveis pelo controle de ponto vejam e autorizem as justificativas apresentadas.</p>
				<p>O acesso direto pelo navegador de internet torna esta ferramenta extremamente dinâmica!</p>
				<ul>
					<li><i class="fas fa-angle-right"></i> Sem instalação e com fácil implantação.</li>
					<li><i class="fas fa-angle-right"></i> Permite que os colaboradores enviem arquivos (atestados, certidões, imagens, etc.), que são armazenados diretamente no banco de dados do iPonto.</li>
					<li><i class="fas fa-angle-right"></i> Possibilidade a maior participação dos gestores no processo de controle do ponto.</li>
					<li><i class="fas fa-angle-right"></i> Torna o sistema de ponto mais transparente aos envolvidos (gestores e funcionários).</li>
					<li><i class="fas fa-angle-right"></i> Interface pode ser acessada pela rede local ou via internet.</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="modulo-mobile">
	<div class="container">
		<div class="row alinhar-vertical iponto">
			<div class="col-lg-8 iponto-texto">
				<h2>iPonto Módulo Mobile</h2><br>
				<p>Permite que os funcionários visualizem e interajam com seu cartão ponto, e que os responsáveis pelo controle de ponto vejam e autorizem as justificativas apresentadas.</p>
				<p>O acesso direto pelo navegador de internet torna esta ferramenta extremamente dinâmica!</p>

			</div>
			<div class="col-lg-4 icone-texto">
			 	<img src="dev/img/mobile2.png" alt="iPonto Módulo Mobile">
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 lista-iponto">
				<ul>
					<li><i class="fas fa-angle-right"></i> Aplicativo auxiliar criado para os colaboradores de empresas que utilizam o iPonto desktop, o qual permitirá o acesso a dados cadastrais pessoais, à marcação complementar por geolocalização e a visualização das marcações complementares já realizadas.</li>
					<li><i class="fas fa-angle-right"></i> Funcionamento em modo off-line* - permite registro da marcação complementar apenas com o GPS do dispositivo.</li>
					<li><i class="fas fa-angle-right"></i> Marcação complementar por geolocalização, em modo on-line (alta precisão e identificação de local) ou off-line (apenas posicionamento global).</li>
					<li><i class="fas fa-angle-right"></i> Controle e segurança da marcação, opcional e personalizável, com senha e/ou foto do colaborador.</li>
					<li><i class="fas fa-angle-right"></i> Limite de marcações em modo off-line. Opcional e configurável.</li>
					<li><i class="fas fa-angle-right"></i> Visualizador de histórico de marcações realizadas, modo individual ou tabela, com opção de filtros de busca.</li>
					<li><i class="fas fa-angle-right"></i> Bloqueio de marcações para funcionários afastados.</li>
					<li><i class="fas fa-angle-right"></i> Alerta configurável que indica o próximo horário de marcação.</li>
					<li><i class="fas fa-angle-right"></i> Modo de segurança que evita o uso em dispositivos com falso posicionamento GPS ativo.</li>
					<li><i class="fas fa-angle-right"></i> Ativação individual por colaborador.</li>
					<li><i class="fas fa-angle-right"></i> Envio automatizado de e-mail, opcional e configurável, com a comprovação da marcação do colaborador, para ele próprio e para seu gestor com o detalhamento das marcações realizadas.</li>
					<li><i class="fas fa-angle-right"></i> Tela de monitoramento de marcações complementares, com visualização do endereço no mapa e da foto da marcação (se obrigatório).</li>
					<li><i class="fas fa-angle-right"></i> Relatório de detalhamento das marcações complementares realizadas por dispositivos móveis.</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="accord">
	<div class="container">
		<div class="w-100">
				<div class="col text-center">
					<div class="section_title">
						<h1>Principais Características</h1>
					</div>
				</div>
			</div>
		<?php
			$i = 0;
            $accordeon = array(
                array('titulo'=>'Banco de dados Firebird','texto'=>'✓ Banco de dados Firebird (gratuito e seguro), personalizado para nossa aplicação;'),
                array('titulo'=>'Instalação simplificada','texto'=>'✓ Instalação simplificada, com opção para multiusuários (estações de trabalho);'),
                array('titulo'=>'Licenciamento por assinatura','texto'=>'✓ Licenciamento por assinatura (período) ou pela compra de direito de uso da licença⁽³⁾;'),
                array('titulo'=>'Sistema de auditoria','texto'=>'✓ Sistema de auditoria, com a rastreabilidade de ações por usuário, data e hora;'),
                array('titulo'=>'Controle de permissões por usuário','texto'=>'✓ Controle de permissões por usuário (telas, ações, empresas, departamentos, etc.);'),
                array('titulo'=>'Serviço automatizado de backup','texto'=>'✓ Serviço automatizado de backup (integrado e gratuito), com opção de gravação de arquivos em nuvem;'),
                array('titulo'=>'Exportação personalizada de dados','texto'=>'✓ Exportação personalizada de dados para qualquer folha de pagamento do mercado;'),
                array('titulo'=>'Tabelas de horário e escala personalizável','texto'=>'✓ Tabelas de horário e escala personalizável, com ajustes de tolerâncias e totalizadores;'),
                array('titulo'=>'Banco de horas configurável','texto'=>'✓ Banco de horas configurável por colaborador, com agendamento de período de aplicação;'),
                array('titulo'=>'Controle programável automatizado','texto'=>'✓ Controle programável automatizado dos períodos de férias e afastamentos;'),
                array('titulo'=>'Relatórios cadastrais e gerenciais','texto'=>'✓ Relatórios cadastrais e gerenciais (totalizadores por funcionário, absenteísmo, aniversariantes, etc);'),
                array('titulo'=>'Tela de apontamento / tratamento','texto'=>'✓ Tela de apontamento / tratamento de ponto integrada – todas as ações são realizadas em um único lugar, com atalhos e cliques rápidos.')
            );
            foreach ($accordeon as $key => $value):
        ?>
		<div class="collap" id="collap" data-toggle="collapse" data-target="#accord<?=$i?>">
			<i class="fas fa-angle-right icone <?= $i == 0 ? "rotate" : "" ?>"></i><a class="accord-amplus" ><?=$value['titulo']?></a>
			<div id="accord<?=$i?>" class="collapse <?= $i == 0 ? "show" : "" ?>">
				<?=$value['texto']?>
			</div>
		</div>
		<?php $i++; endforeach;?>
	</div>
</div>