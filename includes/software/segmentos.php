<div class="container_megasult pb-100">
		<div class="row alinhar-vertical">
			<div class="col-lg-12 text-center mb-60">
				<h1 >Segmentos</h1>
			</div>
			<div class="col-lg-2 col-md-6 nopadding">
				<div class="sistema-box alinhar-vertical">
					<div class="col-lg-4 float-left nopadding">
						<figure class="alinhar-vertical">
							<img src="dev/img/icones/amplus.png" alt="">
						</figure>
					</div>
					<div class="col-lg-8 float-left nopadding">
						<h3>Indústria</h3>
					</div>
				</div>
			</div>
			<div class="col-lg-2 col-md-6 nopadding">
				<div class="sistema-box alinhar-vertical">
					<div class="col-lg-4 float-left nopadding">
						<figure class="alinhar-vertical">
							<img src="dev/img/icones/amplus.png" alt="">
						</figure>
					</div>
					<div class="col-lg-8 float-left nopadding">
						<h3>Distribuição e Atacado</h3>
					</div>
				</div>
			</div>
			<div class="col-lg-2 col-md-6 nopadding">
				<div class="sistema-box alinhar-vertical">
					<div class="col-lg-4 float-left nopadding">
						<figure class="alinhar-vertical">
							<img src="dev/img/icones/amplus.png" alt="">
						</figure>
					</div>
					<div class="col-lg-8 float-left nopadding">
						<h3>Varejo em Geral</h3>
					</div>
				</div>
			</div>
			<div class="col-lg-2 col-md-6 nopadding">
				<div class="sistema-box alinhar-vertical">
					<div class="col-lg-4 float-left nopadding">
						<figure class="alinhar-vertical">
							<img src="dev/img/icones/amplus.png" alt="">
						</figure>
					</div>
					<div class="col-lg-8 float-left nopadding">
						<h3>Prestação de Serviços</h3>
					</div>
				</div>
			</div>
			<div class="col-lg-2 col-md-6 nopadding">
				<div class="sistema-box alinhar-vertical">
					<div class="col-lg-4 float-left nopadding">
						<figure class="alinhar-vertical">
							<img src="dev/img/icones/amplus.png" alt="">
						</figure>
					</div>
					<div class="col-lg-8 float-left nopadding">
						<h3>Folha de Pagamento</h3>
					</div>
				</div>
			</div>
			<div class="col-lg-2 col-md-6 nopadding">
				<div class="sistema-box alinhar-vertical">
					<div class="col-lg-4 float-left nopadding">
						<figure class="alinhar-vertical">
							<img src="dev/img/icones/amplus.png" alt="">
						</figure>
					</div>
					<div class="col-lg-8 float-left nopadding">
						<h3>Controle de Ponto</h3>
					</div>
				</div>
			</div>
			<div class="col-lg-2 col-md-6 nopadding">
				<div class="sistema-box alinhar-vertical">
					<div class="col-lg-4 float-left nopadding">
						<figure class="alinhar-vertical">
							<img src="dev/img/icones/amplus.png" alt="">
						</figure>
					</div>
					<div class="col-lg-8 float-left nopadding">
						<h3>RFID</h3>
					</div>
				</div>
			</div>
			<div class="col-lg-2 col-md-6 nopadding">
				<div class="sistema-box alinhar-vertical">
					<div class="col-lg-4 float-left nopadding">
						<figure class="alinhar-vertical">
							<img src="dev/img/icones/amplus.png" alt="">
						</figure>
					</div>
					<div class="col-lg-8 float-left nopadding">
						<h3>Moda</h3>
					</div>
				</div>
			</div>
			<div class="col-lg-2 col-md-6 nopadding">
				<div class="sistema-box alinhar-vertical">
					<div class="col-lg-4 float-left nopadding">
						<figure class="alinhar-vertical">
							<img src="dev/img/icones/amplus.png" alt="">
						</figure>
					</div>
					<div class="col-lg-8 float-left nopadding">
						<h3>Calçados</h3>
					</div>
				</div>
			</div>
			<div class="col-lg-2 col-md-6 nopadding">
				<div class="sistema-box alinhar-vertical">
					<div class="col-lg-4 float-left nopadding">
						<figure class="alinhar-vertical">
							<img src="dev/img/icones/amplus.png" alt="">
						</figure>
					</div>
					<div class="col-lg-8 float-left nopadding">
						<h3>Perfumaria e Cosméticos</h3>
					</div>
				</div>
			</div>
			<div class="col-lg-2 col-md-6 nopadding">
				<div class="sistema-box alinhar-vertical">
					<div class="col-lg-4 float-left nopadding">
						<figure class="alinhar-vertical">
							<img src="dev/img/icones/amplus.png" alt="">
						</figure>
					</div>
					<div class="col-lg-8 float-left nopadding">
						<h3>Utilidades e Presentes</h3>
					</div>
				</div>
			</div>
			<div class="col-lg-2 col-md-6 nopadding">
				<div class="sistema-box alinhar-vertical">
					<div class="col-lg-4 float-left nopadding">
						<figure class="alinhar-vertical">
							<img src="dev/img/icones/amplus.png" alt="">
						</figure>
					</div>
					<div class="col-lg-8 float-left nopadding">
						<h3>Óticas e Acessórios</h3>
					</div>
				</div>
			</div>
			<div class="col-lg-2 col-md-6 nopadding">
				<div class="sistema-box alinhar-vertical">
					<div class="col-lg-4 float-left nopadding">
						<figure class="alinhar-vertical">
							<img src="dev/img/icones/amplus.png" alt="">
						</figure>
					</div>
					<div class="col-lg-8 float-left nopadding">
						<h3>E-commerce</h3>
					</div>
				</div>
			</div>
			<div class="col-lg-2 col-md-6 nopadding">
				<div class="sistema-box alinhar-vertical">
					<div class="col-lg-4 float-left nopadding">
						<figure class="alinhar-vertical">
							<img src="dev/img/icones/amplus.png" alt="">
						</figure>
					</div>
					<div class="col-lg-8 float-left nopadding">
						<h3>Meios de Pagamento</h3>
					</div>
				</div>
			</div>
		</div>
	</div>