<div class="icon_boxes">
	<div class="container">
		<div class="row alinhar-vertical">
			<div class="col-lg-12" id="texto-contabilidade">
				<div class="icon_box_paragraph text-center">
					<h2 class="text-center">Direito Autoral</h2><br>
					<p class="text-justify">É o direito que o autor de uma obra intelectual tem sobre a sua criação. Com validade de 70 anos contados da morte do autor.</p>
				</div>
			</div>
		</div>
	</div>
</div>