<div class="icon_boxes">
	<div class="container">
		<div class="row alinhar-vertical">
			<div class="col-lg-12" id="texto-contabilidade">
				<div class="icon_box_paragraph text-center">
					<h2 class="text-center">Registro de Software</h2><br>
					<p class="text-justify">No Brasil, qualquer tipo de programa de computador, registrado ou não, tem a sua propriedade intelectual protegida por lei, podendo ser registrada para garantir direitos legais e segurança. O direito de software é de 50 anos contados a partir de 1º. de janeiro do ano subsequente ao da sua publicação ou, na ausência desta, da sua criação.</p>
				</div>
			</div>
		</div>
	</div>
</div>