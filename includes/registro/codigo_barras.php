<div class="icon_boxes">
	<div class="container">
		<div class="row alinhar-vertical">
			<div class="col-lg-12" id="texto-contabilidade">
				<div class="icon_box_paragraph text-center">
					<h2 class="text-center">Código de Barras</h2><br>
					<p class="text-justify">Os códigos de barras são utilizados para representar uma numeração (identificação) atribuída a produtos, unidades logísticas, localizações, ativos fixos e retornáveis, documentos, contêineres, cargas e serviços facilitando a captura de dados através de leitores (scanners) e coletores de código de barras, propiciando a automação de processos trazendo eficiência, maior controle e confiabilidade para a empresa.</p>
				</div>
			</div>
		</div>
	</div>
</div>