<div class="icon_boxes">
	<div class="container">
		<div class="row alinhar-vertical">
			<div class="col-lg-12" id="texto-contabilidade">
				<div class="icon_box_paragraph text-center">
					<h2 class="text-center">Registro de Marcas</h2><br>
					<p class="text-justify">Marca é a representação simbólica de uma entidade, qualquer que ela seja, algo que permite identificá-la de um modo imediato como, por exemplo, um sinal de presença, uma simples pegada. Na teoria da comunicação, pode ser um signo, um símbolo ou um ícone. Uma simples palavra pode referir uma marca. O Registro de marca é valido por 10 anos e prorrogável pelo mesmo prazo consecutivo.</p>
				</div>
			</div>
		</div>
	</div>
</div>