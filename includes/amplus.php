<div class="icon_boxes">
	<div class="container">
		<div class="row alinhar-vertical">
			<div class="w-100">
				<div class="col text-center">
					<div class="section_title">
						<h1>AMPLUS</h1>
					</div>
				</div>
			</div>
			<div class="col-lg-4 segmentos">
				<h2>Segmentos</h2>
				<ul>
					<i class="fas fa-angle-right"></i><li>Indústria</li>
					<i class="fas fa-angle-right"></i><li>Distribuição</li>
					<i class="fas fa-angle-right"></i><li>Materiais de Construção</li>
					<i class="fas fa-angle-right"></i><li>Comércio em geral</li>
					<i class="fas fa-angle-right"></i><li>Prestação de serviços</li>
				</ul>
			</div>

			<div class="col-lg-8" id="texto-amplus">

				<!-- Icon Box Item -->
				<div class="icon_box_paragraph">
					<p>Com amplo conhecimento, oferecemos benefícios reais aos seus negócios, sendo eles: aumento de produtividade, redução dos custos, melhor gestão dos estoques por depósito, melhor administração no processo de compras, melhor definição para precificação de produtos, otimização das rotas e sequência de entregas, análises gráficas em tempo real.</p>
				</div>

			</div>
		</div>
	</div>
</div>

<div class="accord">
	<div class="container">
		<div class="w-100">
				<div class="col text-center">
					<div class="section_title">
						<h1>Recursos disponíveis no AMPLUS</h1>
					</div>
				</div>
			</div>
		<?php
			$i = 0;
            $accordeon = array(
                array('titulo'=>'Indústria','texto'=>'- Engenharia produtos;<br>- Apontamento das operações;<br>- Planejamento de produção;<br>- Controle de produção;<br>- MRP1;<br>- Custos indústrias;<br>- Rastreabilidade das O.Ps;'),	
                array('titulo'=>'WMS','texto'=>'- Gerenciamento de Armazém;<br>- Endereçamento;<br>- Picking;<br>- Separação e Conferência;<br>- Transferência de endereço;'),	
                array('titulo'=>'Contábil Fiscal','texto'=>'- Contábil;<br>- DRE;<br>- Balancete;<br>- Visões contábeis;<br>- Fiscal;<br>- SPED fiscal (Bloco K);<br>- Sintegra, Apurações (ICMS / PIS/ CONFINS e IPI);'),	
                array('titulo'=>'Financeiro','texto'=>'- Contas a receber;<br>- Contas a pagar;<br>- Caixa e Bancos;<br>- Tesouraria;<br>- Fluxo de Caixa;<br>- Controle de Cheques;<br>- Cobrança Escritural;'),	
                array('titulo'=>'Matérias','texto'=>'- Compras;<br>- Recebimento;<br>- Estoque;<br>- Multi – depósitos;<br>- Importador XML;<br>- Equalização;<br>-Curva ABC;<br>- Inventário;<br>- Lote / Validade;'),
                array('titulo'=>'Logística','texto'=>'- Mapa de carga;<br>- Carregamento;<br>- Roteirização;<br>- Ordem de Embarque;<br>- CT-e / MDF-e;<br>- Formação de Embalagens;<br>- Manifesto de Carga;<br>- Conferência Carga;'),		
                array('titulo'=>'Comercial','texto'=>'- Controle Pedidos;<br>- Análise de Crédito;<br>- Pedidos;<br>- Faturamento;<br>- Comissões;<br>- Precificação;<br>- Ranking de Vendas;<br>- Margem de Lucro;<br>- Margem de Contribuição;'),
                array('titulo'=>'Serviços','texto'=>'- Ordem de serviço;<br>- Orçamento;<br>- Aprovação;<br>- Apontamento;<br>- NFS-s;<br>- Histórico de Serviços;<br>- Custo de Serviços;'),
                array('titulo'=>'Frente de Loja','texto'=>'- Venda de Balcão;<br>- NF-e/ NFC-e;<br>- Operações de Caixa;<br>- Abertura;<br>- Sangria;<br>- Reforço;<br>- Fechamento;'),
                array('titulo'=>'Força de vendas','texto'=>'- On-line / Off-line;<br>- Prospecção;<br>- Fidelização;<br>- Televendas;<br>- Controle de Visitas;<br>- Agenda Corporativa;'),
                array('titulo'=>'Folha de pagamento','texto'=>'- Calcula: adiantamento, folha mensal, complementar e 13º salário;<br>- Cálculo específico para multicontratos e centros de custos;<br>- Cálculo de férias individuais, coletivas, em lote, programa e diferença de férias;<br>- Cálculo de rescisões e rescisões complementares;<br>- Gera, controla e monitora, automaticamente as informações ao e-Social;<br>- Gera informações para o e-Social;<br>- Gera informações para o SEFIP, GRRF, CAGED, RAIS, DIRF, HOMOLOGNET, MANAD, TCE, COORDIGUALDADE e SIRETT;<br>- Emite GPS, GRCSU e DARF do IRRF e PIS;<br>- Emissão de cheques;<br>- Diversos relatórios cadastrais e de movimentação;<br>- Extração de dados em diversos formatos;<br>- Emissão do mapa de troco;<br>- Tela de lançamentos online;<br>- Implantação automatizada de empresa;<br>- Importação e geração de arquivo retorno de crédito consignado;<br>- Cadastro e geração de arquivos de pedidos do vale-transporte e alimentação/refeição;<br>- Integração da provisão e movimentações com o módulo contábil;<br>- Integração de informações do ponto eletrônico;<br>- Armazenamento e emissão de informações do PPP;<br>- Provisão de férias e 13º Salário;<br>- Armazenamento de índices e cálculo de ajustes monetários;<br>- Cadastro e controle para lançamento de eventos parcelados;<br>- Editor de relatórios em página cheia, etiquetas e linha/coluna;<br>- Cadastro e apuração de participação nos lucros;<br>- Cadastro e processamento do plano privado de assistência a saúde;<br>- Agendamento para processo automático;<br>- Inicialização automática dos id-procs;<br>- Controle do plano de cargos e salários;<br>- Cadastro e geração automática de lançamentos fixos;<br>- Manutenção do cadastro e geração de arquivos para bancos;<br>- Cadastro e emissão de informações do Banco de Horas;<br>- Mecanismo de transferência de empregados entre empresas;<br>- Controle de alterações salariais;'),
                array('titulo'=>'Recursos gerenciais','texto'=>'- MRP;<br>- Custo Industrial;<br>- Formação do preço de venda;<br>- Mapa de Carga;<br>- Multi depósitos;<br>- Contabilidade gerencial;<br>- Controle de entregas;<br>- Metas e comissões;<br>- Visões gerencias gráficas;<br>- Controle de lote e validade;'),
                array('titulo'=>'Exigências Fiscais','texto'=>'- NF-e;<br>- NFC-e;<br>- CT-e;<br>- MDF-e;<br>- NFS-e;<br>- SPED (FISCAL/BLOCO K /EFD);')
            );
            foreach ($accordeon as $key => $value):
        ?>
		<div class="collap" id="collap" data-toggle="collapse" data-target="#accord<?=$i?>">
			<i class="fas fa-angle-right icone <?= $i == 0 ? "rotate" : "" ?>"></i><a class="accord-amplus" ><?=$value['titulo']?></a>
			<div id="accord<?=$i?>" class="collapse <?= $i == 0 ? "show" : "" ?>">
				<?=$value['texto']?>
			</div>
		</div>
		<?php $i++; endforeach;?>
	</div>
</div>